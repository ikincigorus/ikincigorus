﻿using System.Web;
using System.Web.Optimization;

namespace IkinciGorus
{
    public class BundleConfig
    {
        // For more information on bundling, visit https://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new StyleBundle("~/Content/css").Include(
                      "~/content/css/bootstrap.min.css",
                      "~/content/css/typography.css",
                      "~/content/css/style.css",
                      "~/content/css/responsive.css",
                      "~/content/css/jquery.dataTables.min.css",
                      "~/content/css/jquery-ui.css",
                      "~/content/css/custom.css",
                      "~/content/css/easy-autocomplete.min.css"));

            bundles.Add(new ScriptBundle("~/Content/js").Include(
                        "~/content/js/popper.min.js",
                        "~/content/js/jquery.dataTables.min.js",
                        "~/content/js/bootstrap.min.js",
                        "~/content/js/jquery.appear.js",
                        "~/content/js/countdown.min.js",
                        "~/content/js/waypoints.min.js",
                        "~/content/js/jquery.counterup.min.js",
                        "~/content/js/wow.min.js",
                        "~/content/js/apexcharts.js",
                        "~/content/js/slick.min.js",
                        "~/content/js/select2.min.js",
                        "~/content/js/owl.carousel.min.js",
                        "~/content/js/jquery.magnific-popup.min.js",
                        "~/content/js/smooth-scrollbar.js",
                        "~/content/js/lottie.js",
                        "~/content/js/jquery-ui.min.js",
                        "~/content/js/jquery.maskedinput.js",
                        "~/content/js/chart-custom.js",
                        "~/content/js/jquery.multifile.js",
                        "~/content/js/jquery.easy-autocomplete.min.js",
                        "~/content/js/custom.js"));

            bundles.Add(new ScriptBundle("~/Content/captcha").Include(
                        "~/content/js/captcha.js"));

            bundles.Add(new ScriptBundle("~/Content/broadcast").Include(
                        "~/content/js/broadcast.js"));

            bundles.Add(new ScriptBundle("~/Content/countdown").Include(
                        "~/content/js/countdown.js"));
        }
    }
}
