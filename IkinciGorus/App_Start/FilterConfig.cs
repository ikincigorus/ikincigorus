﻿using IkinciGorus.Models;
using System.Web.Mvc;

namespace IkinciGorus
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new ErrorLogAttribute());
        }
    }
}
