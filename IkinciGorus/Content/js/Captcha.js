﻿$(document).ready(function () {

    $.fn.reImgCaptcha = function reImgCaptcha() {
        $.ajax({
            url: "/login/getcaptchacode?" + new Date().getTime(),
            type: "post",
            contentType: "image/gif",
            success: function (result) {
                $("#imgCaptcha").attr("src", "data:image/gif;base64," + result);
            }
        });
    }

    $('#imgCaptcha').click(function () {
        $.fn.reImgCaptcha();
    });

    $.fn.reImgCaptcha();
});