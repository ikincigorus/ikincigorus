﻿$(document).ready(function () {
    var notifications = function () {
        $.get("/Broadcast/Index", function (data) {
            $('#notifications').html('');
            $('#notifications').html(data.notifications);
            $('#notification-count').html(data.count);
            if (data.count > 0)
                $('#notification-dots').show();
            else
                $('#notification-dots').hide();

            console.log(data);
        });
    }

    setInterval(notifications, 2000);
});