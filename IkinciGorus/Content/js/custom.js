﻿/*
Template: XRay - Responsive Bootstrap 4 Admin Dashboard Template
Author: iqonicthemes.in
Design and Developed by: iqonicthemes.in
NOTE: This file contains the styling for responsive Template.
*/

/*----------------------------------------------
Index Of Script
------------------------------------------------

:: Page Loader
:: Scrollbar
:: Page Menu
:: Header fixed
:: Tooltip
:: Sidebar Widget
:: Magnific Popup
:: Ripple Effect
:: FullScreen
:: Page faq
:: Owl Carousel
:: Select input
:: Search input
:: Counter
:: slick
:: Progress Bar
:: Wow Animation
:: Mailbox
:: Flatpicker
:: chatuser
:: chatuser main
:: Chat
:: todo Page
:: Sidebar Widget


------------------------------------------------
Index Of Script
----------------------------------------------*/

(function (jQuery) {


    "use strict";

    jQuery(document).ready(function () {

        /*---------------------------------------------------------------------
        Page Loader
        -----------------------------------------------------------------------*/
        jQuery("#load").fadeOut();
        jQuery("#loading").delay().fadeOut("");



        /*---------------------------------------------------------------------
        Scrollbar
        -----------------------------------------------------------------------*/
        let Scrollbar = window.Scrollbar;
        if (jQuery('#sidebar-scrollbar').length) {
            Scrollbar.init(document.querySelector('#sidebar-scrollbar'), options);
        }
        let Scrollbar1 = window.Scrollbar;
        if (jQuery('#right-sidebar-scrollbar').length) {
            Scrollbar1.init(document.querySelector('#right-sidebar-scrollbar'), options);
        }



        /*---------------------------------------------------------------------
        Page Menu
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', '.wrapper-menu', function () {
            jQuery(this).toggleClass('open');
            jQuery("body").toggleClass("sidebar-main");
        });



        /*---------------------------------------------------------------------
         Header fixed
         -----------------------------------------------------------------------*/

        jQuery(window).scroll(function () {
            if (jQuery(window).scrollTop() >= 75) {
                jQuery('.iq-top-navbar').addClass('fixed-header');
            } else {
                jQuery('.iq-top-navbar').removeClass('fixed-header');
            }
        });



        /*---------------------------------------------------------------------
        Tooltip
        -----------------------------------------------------------------------*/
        jQuery('[data-toggle="popover"]').popover();
        jQuery('[data-toggle="tooltip"]').tooltip();



        /*---------------------------------------------------------------------
        Sidebar Widget
        -----------------------------------------------------------------------*/
        function checkClass(ele, type, className) {
            switch (type) {
                case 'addClass':
                    if (!ele.hasClass(className)) {
                        ele.addClass(className);
                    }
                    break;
                case 'removeClass':
                    if (ele.hasClass(className)) {
                        ele.removeClass(className);
                    }
                    break;
                case 'toggleClass':
                    ele.toggleClass(className);
                    break;
            }
        }
        jQuery('.iq-sidebar-menu .active').each(function (ele, index) {
            jQuery(this).find('.iq-submenu').parent().addClass('menu-open');
            jQuery(this).find('.iq-submenu').addClass('menu-open');
        })
        jQuery(document).on('click', '.iq-sidebar-menu li', function () {

            if (jQuery(this).hasClass('menu-open')) {
                jQuery(this).find('.iq-submenu').slideUp('slow');
                checkClass(jQuery(this), 'removeClass', 'menu-open');
                if (!jQuery(this).find('.iq-submenu.menu-open .menu-open').length) {
                    checkClass(jQuery(this).find('.menu-open'), 'removeClass', 'menu-open');
                } else {
                    checkClass(jQuery(this).find('.iq-submenu'), 'removeClass', 'menu-open');
                }
            } else if (jQuery(this).find('.iq-submenu').length) {
                jQuery(this).find('.iq-submenu').slideDown('slow');
                checkClass(jQuery(this), 'addClass', 'menu-open');
                checkClass(jQuery(this).find('.iq-submenu'), 'addClass', 'menu-open');
            }
        });


        /*---------------------------------------------------------------------
        Magnific Popup
        -----------------------------------------------------------------------*/
        jQuery('.popup-gallery').magnificPopup({
            delegate: 'a.popup-img',
            type: 'image',
            tLoading: 'Loading image #%curr%...',
            mainClass: 'mfp-img-mobile',
            gallery: {
                enabled: true,
                navigateByImgClick: true,
                preload: [0, 1] // Will preload 0 - before current, and 1 after the current image
            },
            image: {
                tError: '<a href="%url%">The image #%curr%</a> could not be loaded.',
                titleSrc: function (item) {
                    return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
                }
            }
        });
        jQuery('.popup-youtube, .popup-vimeo, .popup-gmaps').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });


        /*---------------------------------------------------------------------
        Ripple Effect
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', ".iq-waves-effect", function (e) {
            // Remove any old one
            jQuery('.ripple').remove();
            // Setup
            let posX = jQuery(this).offset().left,
                posY = jQuery(this).offset().top,
                buttonWidth = jQuery(this).width(),
                buttonHeight = jQuery(this).height();

            // Add the element
            jQuery(this).prepend("<span class='ripple'></span>");


            // Make it round!
            if (buttonWidth >= buttonHeight) {
                buttonHeight = buttonWidth;
            } else {
                buttonWidth = buttonHeight;
            }

            // Get the center of the element
            let x = e.pageX - posX - buttonWidth / 2;
            let y = e.pageY - posY - buttonHeight / 2;


            // Add the ripples CSS and start the animation
            jQuery(".ripple").css({
                width: buttonWidth,
                height: buttonHeight,
                top: y + 'px',
                left: x + 'px'
            }).addClass("rippleEffect");
        });

        /*---------------------------------------------------------------------
        FullScreen
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', '.iq-full-screen', function () {
            let elem = jQuery(this);
            if (!document.fullscreenElement &&
                !document.mozFullScreenElement && // Mozilla
                !document.webkitFullscreenElement && // Webkit-Browser
                !document.msFullscreenElement) { // MS IE ab version 11

                if (document.documentElement.requestFullscreen) {
                    document.documentElement.requestFullscreen();
                } else if (document.documentElement.mozRequestFullScreen) {
                    document.documentElement.mozRequestFullScreen();
                } else if (document.documentElement.webkitRequestFullscreen) {
                    document.documentElement.webkitRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                } else if (document.documentElement.msRequestFullscreen) {
                    document.documentElement.msRequestFullscreen(Element.ALLOW_KEYBOARD_INPUT);
                }
            } else {
                if (document.cancelFullScreen) {
                    document.cancelFullScreen();
                } else if (document.mozCancelFullScreen) {
                    document.mozCancelFullScreen();
                } else if (document.webkitCancelFullScreen) {
                    document.webkitCancelFullScreen();
                } else if (document.msExitFullscreen) {
                    document.msExitFullscreen();
                }
            }
            elem.find('i').toggleClass('ri-fullscreen-line').toggleClass('ri-fullscreen-exit-line');
        });

        /*---------------------------------------------------------------------
        Page faq
        -----------------------------------------------------------------------*/
        jQuery('.iq-accordion .iq-accordion-block .accordion-details').hide();
        jQuery('.iq-accordion .iq-accordion-block:first').addClass('accordion-active').children().slideDown('slow');
        jQuery(document).on("click", '.iq-accordion .iq-accordion-block', function () {
            if (jQuery(this).children('div.accordion-details ').is(':hidden')) {
                jQuery('.iq-accordion .iq-accordion-block').removeClass('accordion-active').children('div.accordion-details ').slideUp('slow');
                jQuery(this).toggleClass('accordion-active').children('div.accordion-details ').slideDown('slow');
            }
        });


        /*---------------------------------------------------------------------
       Owl Carousel
       -----------------------------------------------------------------------*/
        jQuery('.owl-carousel').each(function () {
            let jQuerycarousel = jQuery(this);
            jQuerycarousel.owlCarousel({
                items: jQuerycarousel.data("items"),
                loop: jQuerycarousel.data("loop"),
                margin: jQuerycarousel.data("margin"),
                nav: jQuerycarousel.data("nav"),
                dots: jQuerycarousel.data("dots"),
                autoplay: jQuerycarousel.data("autoplay"),
                autoplayTimeout: jQuerycarousel.data("autoplay-timeout"),
                navText: ["<i class='fa fa-angle-left fa-2x'></i>", "<i class='fa fa-angle-right fa-2x'></i>"],
                responsiveClass: true,
                responsive: {
                    // breakpoint from 0 up
                    0: {
                        items: jQuerycarousel.data("items-mobile-sm"),
                        nav: false,
                        dots: true
                    },
                    // breakpoint from 480 up
                    480: {
                        items: jQuerycarousel.data("items-mobile"),
                        nav: false,
                        dots: true
                    },
                    // breakpoint from 786 up
                    786: {
                        items: jQuerycarousel.data("items-tab")
                    },
                    // breakpoint from 1023 up
                    1023: {
                        items: jQuerycarousel.data("items-laptop")
                    },
                    1199: {
                        items: jQuerycarousel.data("items")
                    }
                }
            });
        });

        /*---------------------------------------------------------------------
        Select input
        -----------------------------------------------------------------------*/

        /*---------------------------------------------------------------------
        Search input
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', function (e) {
            let myTargetElement = e.target;
            let selector, mainElement;
            if (jQuery(myTargetElement).hasClass('search-toggle') || jQuery(myTargetElement).parent().hasClass('search-toggle') || jQuery(myTargetElement).parent().parent().hasClass('search-toggle')) {
                if (jQuery(myTargetElement).hasClass('search-toggle')) {
                    selector = jQuery(myTargetElement).parent();
                    mainElement = jQuery(myTargetElement);
                } else if (jQuery(myTargetElement).parent().hasClass('search-toggle')) {
                    selector = jQuery(myTargetElement).parent().parent();
                    mainElement = jQuery(myTargetElement).parent();
                } else if (jQuery(myTargetElement).parent().parent().hasClass('search-toggle')) {
                    selector = jQuery(myTargetElement).parent().parent().parent();
                    mainElement = jQuery(myTargetElement).parent().parent();
                }
                if (!mainElement.hasClass('active') && jQuery(".navbar-list li").find('.active')) {
                    jQuery('.navbar-list li').removeClass('iq-show');
                    jQuery('.navbar-list li .search-toggle').removeClass('active');
                }

                selector.toggleClass('iq-show');
                mainElement.toggleClass('active');

                e.preventDefault();
            } else if (jQuery(myTargetElement).is('.search-input')) { } else {
                jQuery('.navbar-list li').removeClass('iq-show');
                jQuery('.navbar-list li .search-toggle').removeClass('active');
            }
        });


        /*---------------------------------------------------------------------
        Counter
        -----------------------------------------------------------------------*/
        jQuery('.counter').counterUp({
            delay: 10,
            time: 1000
        });

        /*---------------------------------------------------------------------
        slick
        -----------------------------------------------------------------------*/
        jQuery('.slick-slider').slick({
            centerMode: true,
            centerPadding: '60px',
            slidesToShow: 9,
            slidesToScroll: 1,
            focusOnSelect: true,
            responsive: [{
                breakpoint: 992,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '30',
                    slidesToShow: 3
                }
            }, {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '15',
                    slidesToShow: 1
                }
            }],
            nextArrow: '<a href="#" class="ri-arrow-left-s-line left"></a>',
            prevArrow: '<a href="#" class="ri-arrow-right-s-line right"></a>',
        });

        jQuery('#post-slider').slick({
            centerMode: false,
            centerPadding: '60px',
            slidesToShow: 2,
            slidesToScroll: 1,
            focusOnSelect: true,
            responsive: [{
                breakpoint: 992,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '30',
                    slidesToShow: 1
                }
            }, {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '15',
                    slidesToShow: 1
                }
            }],
            nextArrow: '<a href="#" class="ri-arrow-left-s-line left"></a>',
            prevArrow: '<a href="#" class="ri-arrow-right-s-line right"></a>',
        });

        jQuery('#doster-list-slide').slick({
            centerMode: false,
            centerPadding: '60px',
            slidesToShow: 5,
            slidesToScroll: 1,
            focusOnSelect: true,
            responsive: [{
                breakpoint: 992,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '30',
                    slidesToShow: 3
                }
            }, {
                breakpoint: 480,
                settings: {
                    arrows: false,
                    centerMode: true,
                    centerPadding: '15',
                    slidesToShow: 1
                }
            }],
            nextArrow: '<a href="#" class="ri-arrow-left-s-line left"></a>',
            prevArrow: '<a href="#" class="ri-arrow-right-s-line right"></a>',
        });

        /*---------------------------------------------------------------------
        Progress Bar
        -----------------------------------------------------------------------*/
        jQuery('.iq-progress-bar > span').each(function () {
            let progressBar = jQuery(this);
            let width = jQuery(this).data('percent');
            progressBar.css({
                'transition': 'width 2s'
            });

            setTimeout(function () {
                progressBar.appear(function () {
                    progressBar.css('width', width + '%');
                });
            }, 100);
        });

        jQuery('.progress-bar-vertical > span').each(function () {
            let progressBar = jQuery(this);
            let height = jQuery(this).data('percent');
            progressBar.css({
                'transition': 'height 2s'
            });

            setTimeout(function () {
                progressBar.appear(function () {
                    progressBar.css('height', height + '%');
                });
            }, 100);
        });


        /*---------------------------------------------------------------------
        Wow Animation
        -----------------------------------------------------------------------*/
        let wow = new WOW({
            boxClass: 'wow',
            animateClass: 'animated',
            offset: 0,
            mobile: false,
            live: true
        });
        wow.init();


        /*---------------------------------------------------------------------
        Mailbox
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', 'ul.iq-email-sender-list li', function () {
            jQuery(this).next().addClass('show');
        });

        jQuery(document).on('click', '.email-app-details li h4', function () {
            jQuery('.email-app-details').removeClass('show');
        });

        /*------------------------------------------------------------------
        Flatpicker
        * -----------------------------------------------------------------*/
        if (typeof flatpickr !== 'undefined' && jQuery.isFunction(flatpickr)) {
            jQuery(".flatpicker").flatpickr({
                inline: true
            });
        }

        /*---------------------------------------------------------------------
        chatuser
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', '.chat-head .chat-user-profile', function () {
            jQuery(this).parent().next().toggleClass('show');
        });
        jQuery(document).on('click', '.user-profile .close-popup', function () {
            jQuery(this).parent().parent().removeClass('show');
        });

        /*---------------------------------------------------------------------
        chatuser main
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', '.chat-search .chat-profile', function () {
            jQuery(this).parent().next().toggleClass('show');
        });
        jQuery(document).on('click', '.user-profile .close-popup', function () {
            jQuery(this).parent().parent().removeClass('show');
        });

        /*---------------------------------------------------------------------
        Chat 
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', '#chat-start', function () {
            jQuery('.chat-data-left').toggleClass('show');
        });
        jQuery(document).on('click', '.close-btn-res', function () {
            jQuery('.chat-data-left').removeClass('show');
        });
        jQuery(document).on('click', '.iq-chat-ui li', function () {
            jQuery('.chat-data-left').removeClass('show');
        });
        jQuery(document).on('click', '.sidebar-toggle', function () {
            jQuery('.chat-data-left').addClass('show');
        });

        /*---------------------------------------------------------------------
        todo Page
        -----------------------------------------------------------------------*/
        jQuery(document).on('click', '.todo-task-list > li > a', function () {
            jQuery('.todo-task-list li').removeClass('active');
            jQuery('.todo-task-list .sub-task').removeClass('show');
            jQuery(this).parent().toggleClass('active');
            jQuery(this).next().toggleClass('show');
        });
        jQuery(document).on('click', '.todo-task-list > li li > a', function () {
            jQuery('.todo-task-list li li').removeClass('active');
            jQuery(this).parent().toggleClass('active');
        });

        /*---------------------------------------------------------------------
        Sidebar Widget
        -----------------------------------------------------------------------*/

    });

    var getPriceProtocol = function (id) {
        $.getJSON("/operator/getPriceProtocol/" + id, {}, function (response) {
            var select = $("#priceprotocoluserid");
            select.empty();
            select.append($('<option/>', {
                value: "",
                text: "Seçiniz"
            }));
            $.each(response, function (index, itemData) {
                select.append($('<option/>', {
                    value: itemData.Value,
                    text: itemData.Text
                }));
            });
        });
    }

    $("#priprotsectorid").change(function () {
        getPriceProtocolCalcType($(this).val());
    });

    var getPriceProtocolCalcType = function (id) {
        $.getJSON("/operator/getPriceProtocolCalcType/" + id, {}, function (response) {
            var select = $("#calculationtypeid");
            select.empty();
            select.append($('<option/>', {
                value: "",
                text: "Seçiniz"
            }));
            $.each(response, function (index, itemData) {
                select.append($('<option/>', {
                    value: itemData.Value,
                    text: itemData.Text
                }));
            });
        });
    }

    var getBranch = function (id) {
        $.getJSON("/operator/getBranch/" + id, function (response) {
            var select = $("#branchuserid");
            select.empty();
            select.append($('<option/>', {
                value: "",
                text: "Seçiniz"
            }));
            $.each(response, function (index, itemData) {
                select.append($('<option/>', {
                    value: itemData.Value,
                    text: itemData.Text
                }));
            });
        });
    }

    $(".blocked").change(function (event) {
        event.preventDefault();
        var ctrl = $(this).attr("ctrl");
        $.ajax({
            type: "Post",
            url: "/" + ctrl + "/BlockedUser/" + (ctrl == "Operator" ? $(this).attr("idx") : ""),
            success: function (response) {
                if (ctrl == "Expert") {
                    $("#status-header").html(response);

                }
            }
        });
    });

    jQuery(document).ready(function () {
        jQuery().on('click', '.todo-task-lists li', function () {
            if (jQuery(this).find('input:checkbox[name=todo-check]').is(":checked")) {

                jQuery(this).find('input:checkbox[name=todo-check]').attr("checked", false);
                jQuery(this).removeClass('active-task');
            } else {
                jQuery(this).find('input:checkbox[name=todo-check]').attr("checked", true);
                jQuery(this).addClass('active-task');
            }
        });
    });

    jQuery(document).ready(function () {
        $("#passwordgenerator").click(function () {
            var length = 6;
            var charset = "abcdefghijklnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!*"; 1
            var retVal = "";
            for (var i = 0, n = charset.length; i < length; ++i) {
                retVal += charset.charAt(Math.floor(Math.random() * n));
            }
            $("#password").val(retVal);
        });

        $(".only-numeric").bind("keypress", function (e) {
            var keyCode = e.which ? e.which : e.keyCode

            if (!(keyCode >= 48 && keyCode <= 57)) {
                $(".error").css("display", "inline");
                return false;
            } else {
                $(".error").css("display", "none");
            }
        });

        var dateToday = new Date();
        $(".only-date").datepicker({
            dateFormat: "dd.mm.yy",
            monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
            dayNamesMin: ["Pa", "Pt", "Sl", "Ca", "Pe", "Cu", "Ct"],
            firstDay: 1,
            minDate: dateToday
        });

        $(".only-birth-date").datepicker({
            dateFormat: "dd.mm.yy",
            monthNames: ["Ocak", "Şubat", "Mart", "Nisan", "Mayıs", "Haziran", "Temmuz", "Ağustos", "Eylül", "Ekim", "Kasım", "Aralık"],
            dayNamesMin: ["Pa", "Pt", "Sl", "Ca", "Pe", "Cu", "Ct"],
            firstDay: 1
        });

        $(".only-int").keypress(function (e) {
            alert(e)
            if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
                return false;
            }
        });

        $(".only-phone").mask("(999) 999 9999");

        $(".only-decimal").mask("999.999.999.999,99", { reverse: false });

        $(".only-currency").on("keyup", function (event) {
            // When user select text in the document, also abort.
            var selection = window.getSelection().toString();
            if (selection !== '') {
                return;
            }

            // When the arrow keys are pressed, abort.
            if ($.inArray(event.keyCode, [38, 40, 37, 39]) !== -1) {
                return;
            }

            var $this = $(this);
            // Get the value.
            var input = $this.val();
            var input = input.replace(/[\D\s\._\-]+/g, "");
            input = input ? parseInt(input, 10) : 0;
            $this.val(function () {
                return (input === 0) ? "" : input.toLocaleString("tr-TR");
            });
        });

        $(".nextBtn").click(function () {
            var files = [];
            $("#patientname_span").html($("#patientname").val());
            $("#institutionpatientid_span").html($("#institutionpatientid").val());
            $("#hospitalid_span").html($("#hospitalid option:selected").text());
            $("#branchfileid_span").html($("#branchfileid option:selected").text());
            $("#demandamount_span").html($("#demandamount").val() + " ₺");
            $("#filedescription_span").html("<br>" + $("#filedescription").val().replace("\r\n", "<br>"));
            $("#contents_span").html($("#contents_span").prefunction());
            $("#filecargo_span").html($("#filecargo").is(':checked') ? "Var" : "Yok");
            $("#fileupload_span").html($("#fileupload").val())
        });

        $.fn.prefunction = function () {
            var tmpHTML = "<br><table style='width:50%;'>";
            $("#contents_wrap_list span").each(function () {
                tmpHTML += "<tr><td>" + $(this).html() + "</td></tr>";
            });
            return tmpHTML += "</table>";
        };

        $('.pass').click(function (event) {
            event.preventDefault();
            if (confirm('Şifre resetlenecek ve mail adresine gönderilecek ?')) {
                $.ajax({
                    type: 'Post',
                    url: $(this).attr('href'),
                    success: function (response) {
                        alert(response);
                    }
                });
            }
        });

        var is = 0;

        var hidesectorfunction = function (id) {

            if ($("#usertitleid").val() == 1 && $("#usersectoridc").val() != "" && $("#usersectoridc").val() != null) {
                if (is == 1) {
                    getPriceProtocol($("#usersectoridc").val());
                    getBranch($("#usersectoridc").val());
                }
            }

            if ($("#usersectoridc").val() == null)
                $("#usersectorid").val(1);
            else
                $("#usersectorid").val($("#usersectoridc").val());

            is = 1;
        }

        hidesectorfunction($("#usersectoridc").val());

        $("#usersectoridc").change(function () {
            hidesectorfunction($(this).val());
        });

        var hidetitlefunction = function (id) {

            if (id == 1) {
                $("#priceprotocoluserid").prop("disabled", false);
                $("#branchuserid").prop("disabled", false);
                $("#customerowner").prop("disabled", false);
                $("#manage").prop("disabled", true);
                $("#manage").prop("checked", false);
                $("#priceprotocoluserid").prop("required", true);
                $("#branchuserid").prop("required", true);
                /*$("#usersectoridc option[value='']").prop('disabled', false);*/
                $("#usersectoridc option[value*=1]").prop('disabled', true);
                /*$("#usersectoridc option[value*=2]").prop('disabled', false);
                $("#usersectoridc option[value*=3]").prop('disabled', false);
                $("#usersectoridc option[value*=4]").prop('disabled', false);*/
                if ($("#usersectoridc").val() == null)
                    $("#usersectoridc").prop("selectedIndex", 0);
                $("#usersectoridc").prop("disabled", false);
            }
            else if (id == 2) {
                $("#priceprotocoluserid").prop("disabled", true);
                $("#branchuserid").prop("disabled", true);
                $("#customerowner").prop("disabled", true);
                $("#manage").prop("disabled", false);
                $("#priceprotocoluserid").prop("selectedIndex", 0);
                $("#branchuserid").prop("selectedIndex", 0);
                $("#priceprotocoluserid").prop("required", false);
                $("#branchuserid").prop("required", false);
                /*$("#usersectoridc option[value='']").prop('disabled', false);*/
                $("#usersectoridc option[value*=1]").prop('disabled', true);
                /*$("#usersectoridc option[value*=2]").prop('disabled', false);
                $("#usersectoridc option[value*=3]").prop('disabled', false);
                $("#usersectoridc option[value*=4]").prop('disabled', false);*/
                if ($("#usersectoridc").val() == null)
                    $("#usersectoridc").prop("selectedIndex", 0);
                $("#usersectoridc").prop("disabled", false);
            }
            else if (id == 3) {
                $("#priceprotocoluserid").prop("disabled", true);
                $("#branchuserid").prop("disabled", true);
                $("#customerowner").prop("disabled", false);
                $("#manage").prop("disabled", false);
                $("#priceprotocoluserid").prop("selectedIndex", 0);
                $("#branchuserid").prop("selectedIndex", 0);
                $("#priceprotocoluserid").prop("required", false);
                $("#branchuserid").prop("required", false);
                $("#usersectoridc").prop("selectedIndex", 1);
                /*$("#usersectoridc option[value='']").prop('disabled', true);
                $("#usersectoridc option[value*=1]").prop('disabled', true);
                $("#usersectoridc option[value*=2]").prop('disabled', true);
                $("#usersectoridc option[value*=3]").prop('disabled', true);
                $("#usersectoridc option[value*=4]").prop('disabled', true);*/
                $("#usersectoridc").prop("disabled", true);
            }
            else {
                $("#usersectoridc").prop("disabled", true);
            }
            if ($("#usersectoridc").val() == null)
                $("#usersectorid").val(1);
            else
                $("#usersectorid").val($("#usersectoridc").val());
        }

        hidetitlefunction($("#usertitleid").val());

        $("#usertitleid").change(function () {
            hidetitlefunction($(this).val());
            hidesectorfunction($("#usersectoridc").val());
        });

        $("#filecargo").change(function () {
            if (this.checked) {
                $(".cargo-address").show();
            }
            else {
                $(".cargo-address").hide();
            }
        });
    });

})(jQuery);