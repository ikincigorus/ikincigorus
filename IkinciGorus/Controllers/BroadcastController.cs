﻿using IkinciGorus.Models;
using System.Linq;
using System.Web.Mvc;

namespace IkinciGorus.Controllers
{
    public class BroadcastController : Controller
    {
        ContextDb db = new ContextDb();
        _Model site = new _Model();

        private static string notifications = "";
        private static long count = 0;
        string[] Statu = { "" };

        [ErrorLog]
        [Authorize]
        public JsonResult Index()
        {
            if (_Users.CurrentTitle == 1)
            {
                Statu = new string[] { "15" };
                site.M_BROADCAST_FILES = db.T_FILES.Where(t => t.WorkingUserId == _Users.CurrentId && Statu.Contains(t.T_STATUS.StatuCode.ToString()) && t.FileRead == true).OrderByDescending(t => t.UpdateDate).ToList();
                site.Controller = "Expert";
            }
            else if (_Users.CurrentTitle == 2)
            {
                Statu = new string[] { "5", "10", "30", "99" };
                site.M_BROADCAST_FILES = db.T_FILES.Where(t => t.CreaterCustomerId == _Users.CurrentCustomerId && Statu.Contains(t.T_STATUS.StatuCode.ToString()) && t.FileRead == true).OrderByDescending(t => t.UpdateDate).ToList();
                site.Controller = "Customer";
            }
            else
            {
                Statu = new string[] { "25" };
                site.M_BROADCAST_FILES = db.T_FILES.Where(t => Statu.Contains(t.T_STATUS.StatuCode.ToString()) && t.FileRead == true).OrderByDescending(t => t.UpdateDate).ToList();
                site.Controller = "Operator";
            }

            notifications = "";

            if (site.M_BROADCAST_FILES.Count > 0)
            {
                foreach (var item in site.M_BROADCAST_FILES)
                {
                    notifications += "<a href=\"/" + site.Controller + "/FilesDetails/"  + item.Id + "\" class=\"iq-sub-card\">";
                    notifications += "<div class=\"media align-items-center\">";
                    notifications += "<div class=\"\">";
                    notifications += "<img class=\"avatar-40 rounded\" src=\"/Content/images/user/01.jpg\" alt=\"\">";
                    notifications += "</div>";
                    notifications += "<div class=\"media-body ml-3\">";
                    notifications += "<h6 class=\"mb-0\">" + System.Text.RegularExpressions.Regex.Replace(item.PatientName, @"[a-z,ğ,ç,ş,ü]", m => " * ") + "</h6>";
                    notifications += "<small class=\"float-right font-size-12\">" + item.CreateDate + "</small>";
                    notifications += "</div>";
                    notifications += "</div>";
                    notifications += "</a>";
                }
            }
            else
            {
                notifications += "<div class=\"iq-sub-card\">";
                notifications += "<div class=\"media align-items-center\">";
                notifications += "<div class=\"media-body ml-3\">";
                notifications += "<h6 class=\"mb-0 text-center\">Bildiriminiz yok</h6>";
                notifications += "</div>";
                notifications += "</div>";
                notifications += "</div>";
            }

            count = site.M_BROADCAST_FILES.Count;

            return Json(new { notifications, count }, JsonRequestBehavior.AllowGet);
        }
    }
}