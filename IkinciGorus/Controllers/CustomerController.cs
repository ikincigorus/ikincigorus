﻿using IkinciGorus.Enums;
using IkinciGorus.Models;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.tool.xml;
using System;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;

namespace IkinciGorus.Controllers
{
    public class CustomerController : Controller
    {
        _Model site = new _Model();
        ContextDb db = new ContextDb();
        string[] Statu = { "" };

        [ErrorLog]
        [Authorize(Roles = "2")]
        public ActionResult Dashboard()
        {
            BroadCast();
            site.M_STATUS = db.T_STATUS.ToList();
            site.M_FILES = db.T_FILES.Where(t => t.CreaterCustomerId == _Users.CurrentCustomerId).ToList();
            return View(site);
        }

        #region Files

        [ErrorLog]
        [Authorize(Roles = "2")]
        public ActionResult Files()
        {
            BroadCast();
            site.M_FILES = db.T_FILES.Where(t => t.CreaterCustomerId == _Users.CurrentCustomerId && t.T_STATUS.StatuCode < 99).OrderByDescending(t => t.UpdateDate)
                .ToList();
            site.M_STATUS = db.T_STATUS.ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "2")]
        public ActionResult FilesClosed()
        {
            BroadCast();
            site.M_FILES = db.T_FILES.Where(t => t.CreaterCustomerId == _Users.CurrentCustomerId && t.T_STATUS.StatuCode == 99).ToList();
            site.M_STATUS = db.T_STATUS.ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "2")]
        public ActionResult SelectSector()
        {
            BroadCast();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
            site.M_CUSTOMER = db.T_CUSTOMER.Where(t => t.Id == _Users.CurrentCustomerId).FirstOrDefault();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "2")]
        public ActionResult NewFiles(int id)
        {
            BroadCast();
            string sec = Convert.ToString(id);
            site.M_CUSTOMER = db.T_CUSTOMER.Where(t => t.Id == _Users.CurrentCustomerId && t.CustomerSector.Contains(sec) == true).FirstOrDefault();
            if (site.M_CUSTOMER == null)
                return Redirect("/Customer/SelectSector");
            site.M_HOSPITALS = db.T_HOSPITAL.Where(t => t.Status == 1).OrderBy(o => o.TitleName).ToList();
            site.M_BRANCHS = db.T_BRANCH.Where(t => t.BranchSectorId == id && t.Status == 1).OrderBy(o => o.BranchName).ToList();
            site.M_SECTORLABEL = _Common.SectorLabel(id);
            site.SectorId = id;
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "2")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewFiles(T_FILES_R File, HttpPostedFileBase[] contents, int id)
        {
            int sts = 0;

            T_CUSTOMER customerData = db.T_CUSTOMER.Where(t => t.Id == _Users.CurrentCustomerId).FirstOrDefault();

            T_FILES Files = new T_FILES();

            // linkten gelen id - sector id
            Files.FilesSectorId = id;

            Files.PatientName = _Common.MaskName(File.PatientName);
            // System.Text.RegularExpressions.Regex.Replace(File.PatientName, @"[a-z,ğ,ç,ş,ü]", m => "*");

            Files.InstitutionPatientId = File.InstitutionPatientId;
            Files.HospitalId = File.HospitalId;
            Files.BranchFileId = File.BranchFileId;
            Files.CreaterUserId = _Users.CurrentId;
            Files.CreaterCustomerId = _Users.CurrentCustomerId;
            Files.FileDescription = File.FileDescription;
            Files.DoctorDescription = _Common.SectorExpertComment(id).Rows[0][1].ToString();
            Files.FileWaiting = false;
            decimal demandAmount = Convert.ToDecimal(File.DemandAmount.Replace(".", ""));
            Files.DemandAmount = demandAmount;
            Files.EvaluatedAmount = 0;
            if (File.FileCargo == true)
            {
                sts = 1;
                Files.FileCargo = false;
            }
            else
            {
                sts = 2;
                Files.FileCargo = false;
            }

            Files.FileUpload = File.FileUpload;

            Files.IsFree = false;

            long? priceProtocolId = 0;
            if (Files.FilesSectorId == SectorEnum.HUKKUK)
                priceProtocolId = customerData.PriceProtocolLawId;
            if (Files.FilesSectorId == SectorEnum.SAGLIK)
                priceProtocolId = customerData.PriceProtocolHealthId;
            if (Files.FilesSectorId == SectorEnum.SIGORTA)
                priceProtocolId = customerData.PriceProtocolInsuranceId;

            T_PRICEPROTOCOL priceProtocol = db.T_PRICEPROTOCOL.Where(t => t.Id == priceProtocolId).FirstOrDefault();

            Files.Price1 = _Common.PriceProtocolCalc(priceProtocol.CalculationTypeId, priceProtocol.UnitPrice, demandAmount);
            Files.Price2 = 0;
            Files.Status = sts;
            Files.FileRead = true;
            Files.Rating = 0;
            Files.RollBackFile = false;
            Files.IsCancel = false;
            Files.CreateDate = DateTime.Now;
            Files.UpdateDate = DateTime.Now;
            db.T_FILES.Add(Files);

            if (contents[0] != null)
            {
                foreach (HttpPostedFileBase item in contents)
                {
                    byte[] Content;
                    using (BinaryReader br = new BinaryReader(item.InputStream))
                    {
                        Content = br.ReadBytes(item.ContentLength);
                    }

                    T_DOCUMENTS Docs = new T_DOCUMENTS();
                    Docs.FileId = Files.Id;
                    Docs.ContentName = item.FileName;
                    Docs.ContentType = item.ContentType;
                    Docs.Contents = Content;
                    Docs.CreateDate = DateTime.Now;
                    Docs.UpdateDate = DateTime.Now;
                    db.T_DOCUMENTS.Add(Docs);

                }
            }

            db.SaveChanges();

            return Redirect("/Customer/Files");
        }

        [ErrorLog]
        [Authorize(Roles = "2")]
        public ActionResult FilesDetails(long id)
        {
            BroadCast();
            site.M_FILE = db.T_FILES.Where(t => t.Id == id && t.CreaterCustomerId == _Users.CurrentCustomerId).FirstOrDefault();
            site.M_DOCUMENTS = db.T_DOCUMENTS.Where(t => t.FileId == id && t.T_FILES.CreaterCustomerId == _Users.CurrentCustomerId).ToList();
            site.M_STATUS = db.T_STATUS.ToList();

            site.M_SECTORLABEL = _Common.SectorLabel(site.M_FILE.FilesSectorId);
            site.SectorId = site.M_FILE.FilesSectorId;

            site.link = (site.M_FILE.Status == 7 ? "filesclosed" : "fileslist");

            string[] Statu = new string[] { "5", "10", "30", "99" };
            if (site.M_FILE.FileRead == true && Statu.Contains(site.M_FILE.T_STATUS.StatuCode.ToString()))
            {
                site.M_FILE.FileRead = false;
                db.SaveChanges();
            }

            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "2")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilesDetails(int? id, string evaluatedamount, int? rating, string ratingdescription, string button)
        {
            if (button == "closedfile")
            {

                if (rating < 1)
                    rating = 1;
                else if (rating > 10)
                    rating = 10;

                T_FILES files = db.T_FILES.Where(t => t.Id == id && t.T_STATUS.StatuCode == 30).FirstOrDefault();
                files.FileWaiting = false;
                files.Status = 7;
                files.FileRead = true;
                files.Rating = rating;
                files.RatingDescription = ratingdescription;
                files.UpdateDate = DateTime.Now;

                decimal evalAmount = Convert.ToDecimal(evaluatedamount);
                files.EvaluatedAmount = (evalAmount > files.DemandAmount ? files.DemandAmount : evalAmount);

                T_USER_STATS ustats = db.T_USER_STATS.Where(t => t.UserId == files.WorkingUserId).FirstOrDefault();
                ustats.ClosedFileCount += 1;
                ustats.TotalRating += rating;
                ustats.LastUpdateDate = DateTime.Now;

                T_CUSTOMER_STATS cstats = db.T_CUSTOMER_STATS.Where(t => t.CustomerId == files.CreaterCustomerId).FirstOrDefault();
                cstats.ClosedFileCount += 1;
                cstats.TotalRating += rating;
                cstats.LastUpdateDate = DateTime.Now;

                db.SaveChanges();
            }
            else if (button == "rollbackfile")
            {
                T_FILES files = db.T_FILES.Where(t => t.Id == id).FirstOrDefault();

                if (files.RollBackFile == false)
                    files.RollBackFile = true;
                else
                    files.RollBackFile = false;

                files.UpdateDate = DateTime.Now;
                db.SaveChanges();
            }
            else if (button == "iscancel")
            {
                T_FILES files = db.T_FILES.Where(t => t.Id == id).FirstOrDefault();

                files.IsCancel = true;
                files.RollBackFile = false;
                files.Status = 7;
                files.UpdateDate = DateTime.Now;

                db.SaveChanges();
            }


            return Redirect("/Customer/Files");
        }

        [ErrorLog]
        [Authorize(Roles = "2")]
        public FileResult DownloadFile(long id)
        {
            T_DOCUMENTS doc = db.T_DOCUMENTS.Where(t => t.Id == id && t.T_FILES.CreaterCustomerId == _Users.CurrentCustomerId).FirstOrDefault();
            return File(doc.Contents, doc.ContentType, doc.ContentName);
        }

        [ErrorLog]
        [Authorize(Roles = "2")]
        public ActionResult CreatePdf(long id)
        {
            site.M_FILE = db.T_FILES.Where(t => t.CreaterCustomerId == _Users.CurrentCustomerId && t.Id == id).FirstOrDefault();
            site.M_SECTORLABEL = _Common.SectorLabel(site.M_FILE.FilesSectorId);
            _CreatePdf.GenerateInvoice(site.M_FILE, site.M_SECTORLABEL);
            return View();
        }

        [ErrorLog]
        [Authorize(Roles = "2")]
        public ActionResult ClearFileInfo(long id)
        {
            site.M_DOCUMENTS = db.T_DOCUMENTS.Where(t => t.FileId == id).ToList();
            for(int i = 0; i < site.M_DOCUMENTS.Count; i++)
            {
                T_DOCUMENTS document = site.M_DOCUMENTS[i];
                document.ContentName = document.ContentName + " - Silindi";
                document.Contents = null;
                document.UpdateDate = DateTime.Now;

                db.SaveChanges();
            }

            return Redirect("/Customer/FilesDetails/" + id);
        }

        #endregion

        #region Users

        [ErrorLog]
        [Authorize(Roles = "2")]
        public ActionResult Users()
        {
            BroadCast();
            site.M_USERS = db.T_USER.Where(t => t.CustomerId == _Users.CurrentCustomerId).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "2")]
        public ActionResult NewUsers()
        {
            BroadCast();
            site.M_USER = new T_USER();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "2")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewUsers(T_USER User)
        {
            T_USER UsrControl = db.T_USER.Where(t => (t.Phone == User.Phone || t.Email == User.Email) && t.Status == 1).FirstOrDefault();

            if (UsrControl != null)
            {
                ViewBag.ErrorMessage = "";
                if (UsrControl.Email == User.Email)
                {
                    ViewBag.ErrorMessage += UsrControl.Email + " mail adresi sistemde mevcut. <br>";
                }
                if (UsrControl.Phone == User.Phone)
                {
                    ViewBag.ErrorMessage += UsrControl.Phone + " telefon numarası sistemde mevcut. <br>";
                }

                site.M_USER = User;
                return View(site);
            }
            else
            {
                T_USER Usr = new T_USER();
                Usr.UserGuid = Guid.NewGuid().ToString();
                Usr.UserSectorId = _Users.CurrentSectorId;
                Usr.Name = User.Name;
                Usr.UserTitleId = 2;
                Usr.Gender = User.Gender;
                Usr.BirthDate = User.BirthDate;
                Usr.Phone = User.Phone;
                Usr.Email = User.Email;
                Usr.Status = User.Status;
                Usr.CustomerId = _Users.CurrentCustomerId;
                Usr.Manage = false;
                Usr.CustomerOwner = false;
                Usr.Blocked = true;
                Usr.CreateDate = DateTime.Now;
                Usr.UpdateDate = DateTime.Now;

                string password = _Common.CreatePassword(6);
                string passwordhash = _Common.GetHashString(password);
                Usr.Password = passwordhash;
                Usr.PasswordCount = 3;
                Usr.PasswordSetDate = DateTime.Now;
                db.T_USER.Add(Usr);

                T_USER_STATS ustats = new T_USER_STATS();
                ustats.UserId = Usr.Id;
                ustats.ClosedFileCount = 0;
                ustats.TotalRating = 0;
                ustats.LastUpdateDate = DateTime.Now;
                db.T_USER_STATS.Add(ustats);

                db.SaveChanges();

                _Common.SendMail(User.Email, "İkinci Göz sistemi kullanıcı şifreniz", "Merhaba " + User.Name + "<br><br>İkinci göz sistemi için şifreniz : " + password);

                return Redirect("/Customer/Users");
            }
        }

        [ErrorLog]
        [Authorize(Roles = "2")]
        public ActionResult EditUsers(long id)
        {
            BroadCast();
            site.M_USER = db.T_USER.Where(t => t.Id == id && t.CustomerId == _Users.CurrentCustomerId).FirstOrDefault();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "2")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUsers(long id, T_USER User)
        {
            T_USER UsrControl = db.T_USER.Where(t => (t.Phone == User.Phone || t.Email == User.Email) && t.Id != id && t.Status == 1).FirstOrDefault();

            if (UsrControl != null)
            {
                ViewBag.ErrorMessage = "";
                if (UsrControl.Email == User.Email)
                {
                    ViewBag.ErrorMessage += UsrControl.Email + " mail adresi sistemde mevcut. <br>";
                }
                if (UsrControl.Phone == User.Phone)
                {
                    ViewBag.ErrorMessage += UsrControl.Phone + " telefon numarası sistemde mevcut. <br>";
                }

                site.M_USER = User;
                return View(site);
            }
            else
            {
                T_USER Usr = db.T_USER.Where(t => t.Id == id && t.CustomerId == _Users.CurrentCustomerId).FirstOrDefault();
                Usr.Name = User.Name;
                Usr.Gender = User.Gender;
                Usr.BirthDate = User.BirthDate;
                Usr.Phone = User.Phone;
                Usr.Email = User.Email;
                Usr.Status = User.Status;
                Usr.UpdateDate = DateTime.Now;
                db.SaveChanges();

                return Redirect("/Customer/Users");
            }
        }

        [ErrorLog]
        [Authorize(Roles = "2")]
        public ActionResult DeleteUsers(long id)
        {
            T_USER Usr = db.T_USER.Where(t => t.Id == id && t.CustomerId == _Users.CurrentCustomerId).FirstOrDefault();
            Usr.Status = 0;
            db.SaveChanges();
            return Redirect("/Customer/Users");
        }

        #endregion

        #region ChangePassword

        [ErrorLog]
        [Authorize(Roles = "2")]
        public ActionResult ChangePassword()
        {
            BroadCast();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "2")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(string OldPassword, string NewPassword, string ConfirmNewPassword)
        {
            T_USER User = db.T_USER.Where(t => t.Id == _Users.CurrentId && t.Status == 1).FirstOrDefault();

            if (_Common.GetHashString(OldPassword) == User.Password)
            {

                if (NewPassword == ConfirmNewPassword)
                {
                    ViewBag.ErrorMessage = "Yeni şifreleriniz birbiriyle uyumsuz!";
                }
                else
                {
                    ViewBag.ErrorMessage = "Şifreniz değiştirildi ve mail adresinize gönderildi.";
                    string passwordhash = _Common.GetHashString(NewPassword);
                    User.Password = passwordhash;
                    User.PasswordCount = 3;
                    User.PasswordSetDate = DateTime.Now;
                    db.SaveChanges();
                    _Common.SendMail(User.Email, "İkinci göz sistemi kullanıcı şifreniz yenilendi", "Merhaba " + User.Name + "<br><br>İkinci göz sistemi için şifreniz : " + NewPassword);

                }
            }
            else
            {
                ViewBag.ErrorMessage = "Eski şifreniz hatalı!";
            }

            return View(site);
        }

        #endregion

        #region Statistics

        [ErrorLog]
        [Authorize(Roles = "2")]
        public ActionResult CustomerSatisfaction()
        {
            site.M_CUSTOMER_STAT = db.T_CUSTOMER_STATS.Where(t => t.CustomerId == _Users.CurrentCustomerId).FirstOrDefault();
            if (site.M_CUSTOMER_STAT != null && site.M_CUSTOMER_STAT.ClosedFileCount > 0)
                site.StatsRound = site.M_CUSTOMER_STAT.TotalRating / site.M_CUSTOMER_STAT.ClosedFileCount;
            else
                site.StatsRound = 0;

            return View(site);
        }

        #endregion

        #region Reports

        [ErrorLog]
        [Authorize(Roles = "2")]
        public ActionResult CostPaymentReport()
        {
            BroadCast();
            string month1 = "";
            string currency1 = "";
            string month2 = "";
            string currency2 = "";
            decimal cumulated = 0;
            site.M_FILES = db.T_FILES.Where(t => t.CreaterCustomerId == _Users.CurrentCustomerId && t.Status == 7 && t.IsCancel == false).OrderByDescending(t => t.UpdateDate).ToList();
            site.CostPayment = site.M_FILES.Where(s => s.IsFree == false && s.IsCancel == false).Sum(t => t.Price1);
            site.ClosedFiles = site.M_FILES.Count();
            site.M_CUSTOMER_STAT = db.T_CUSTOMER_STATS.Where(t => t.CustomerId == _Users.CurrentCustomerId).FirstOrDefault();
            site.Rating = (site.M_CUSTOMER_STAT.ClosedFileCount == 0 ? 0 : site.M_CUSTOMER_STAT.TotalRating / site.M_CUSTOMER_STAT.ClosedFileCount);

            var data = db.T_FILES.Where(t => t.CreaterCustomerId == _Users.CurrentCustomerId && t.Status == 7 && t.IsCancel == false).Select(k => new { k.UpdateDate.Value.Year, k.UpdateDate.Value.Month, k.Price1, k.IsFree }).GroupBy(x => new { x.Year, x.Month, x.IsFree }, (key, group) => new
            {
                yr = key.Year,
                mnth = key.Month,
                tCharge = group.Sum(k => k.Price1),
                isFree = key.IsFree
            }).ToList();

            site.M_REPORT_PROGRESS = _Common.ToDataTable(data);
            site.M_REPORT_CUMULATED = _Common.ToDataTable(data);

            for (int i = 0; i < site.M_REPORT_PROGRESS.Rows.Count; i++)
            {
                month1 += "'" + _Common.GetMonth(site.M_REPORT_PROGRESS.Rows[i][1].ToString()) + "'" + ",";
                if ((bool)site.M_REPORT_PROGRESS.Rows[i][3] == false)
                {
                    currency1 += site.M_REPORT_PROGRESS.Rows[i][2].ToString().Replace(',', '.') + ",";
                    cumulated += Convert.ToDecimal(site.M_REPORT_CUMULATED.Rows[i][2]);
                }
                else
                {
                    currency1 += "0,";
                    cumulated += 0;
                }
                month2 += "'" + _Common.GetMonth(site.M_REPORT_CUMULATED.Rows[i][1].ToString()) + "'" + ",";
                currency2 += cumulated.ToString().Replace(',', '.') + ",";
            }

            site.month1 = month1.Length > 0 ? (month1.Remove(month1.Length - 1)) : "";
            site.currency1 = currency1.Length > 0 ? currency1.Remove(currency1.Length - 1) : "";

            site.month2 = month2.Length > 0 ? month2.Remove(month2.Length - 1) : "";
            site.currency2 = currency2.Length > 0 ? currency2.Remove(currency2.Length - 1) : "";

            return View(site);
        }

        [ErrorLog]
        public ActionResult FilePdf(long id)
        {
            BroadCast();
            site.M_FILE = db.T_FILES.Where(t => t.Id == id).FirstOrDefault();
            site.M_DOCUMENTS = db.T_DOCUMENTS.Where(t => t.FileId == id).ToList();
            site.M_STATUS = db.T_STATUS.ToList();

            site.M_SECTORLABEL = _Common.SectorLabel(site.M_FILE.FilesSectorId);
            site.SectorId = site.M_FILE.FilesSectorId;

            return View(site);
        }

        #endregion

        #region Advice

        [ErrorLog]
        [Authorize(Roles = "2")]
        public ActionResult Advice()
        {
            return View();
        }

        [ErrorLog]
        [HttpPost]
        [Authorize(Roles = "2")]
        public JsonResult Advice(string AdviceExp)
        {
            string ok = "0";
            
            if (AdviceExp != "")
            {
                T_ADVICE adv = new T_ADVICE();
                adv.AdviceUserId = _Users.CurrentId;
                adv.AdviceExp = AdviceExp;
                adv.IsRead = false;
                adv.CreateDate = DateTime.Now;
                db.T_ADVICE.Add(adv);
                db.SaveChanges();

                ok = "1";

                _Common.SystemSendMail("İkinci Göz sistemi - Fikir ve Tavsiyeler", "İkinci göz sistemi için tavsiye öneri geldi.<br><br>Id : " + adv.Id + "<br><br>Fikir Detayı<br>-----------------------------<br>" + AdviceExp);
            }
            return Json(ok);
        }

        #endregion

        [ErrorLog]
        [Authorize(Roles = "2")]
        public JsonResult getFileSearch(string id)
        {
            return Json(db.T_FILES.Where(t => t.CreaterCustomerId == _Users.CurrentCustomerId && t.Id.ToString().StartsWith(id)).OrderBy(o => o.Id).Select(s => new { Id = s.Id + "( " + s.T_STATUS.StatuName + " )", Link = "/Customer/FilesDetails/" + s.Id }), JsonRequestBehavior.AllowGet);
        }

        [ErrorLog]
        private void BroadCast()
        {
            Statu = new string[] { "5", "10", "30", "99" };
            site.M_BROADCAST_FILES = db.T_FILES.Where(t => t.CreaterCustomerId == _Users.CurrentCustomerId && Statu.Contains(t.T_STATUS.StatuCode.ToString()) && t.FileRead == true).OrderByDescending(t => t.UpdateDate).ToList();
            site.Controller = "Customer";
        }
    }
}