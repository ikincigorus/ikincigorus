﻿using IkinciGorus.Models;
using System;
using System.Data;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;

namespace IkinciGorus.Controllers
{
    public class ExpertController : Controller
    {
        _Model site = new _Model();
        ContextDb db = new ContextDb();
        string[] Statu = { "" };

        [ErrorLog]
        [Authorize(Roles = "1")]
        public ActionResult Dashboard()
        {
            BroadCast();
            site.M_FILES = db.T_FILES.Where(t => t.WorkingUserId == _Users.CurrentId && t.T_STATUS.StatuCode == 15).ToList();
            site.RejectDay = Convert.ToInt16(WebConfigurationManager.AppSettings["jobRejectedDay"]);
            return View(site);
        }

        #region Files

        [ErrorLog]
        [Authorize(Roles = "1")]
        public ActionResult EditFiles(int id)
        {
            BroadCast();
            site.M_FILE = db.T_FILES.Where(t => t.Id == id && t.WorkingUserId == _Users.CurrentId && t.T_STATUS.StatuCode == 15).FirstOrDefault();
            site.M_DOCUMENTS = db.T_DOCUMENTS.Where(t => t.FileId == id && t.T_FILES.WorkingUserId == _Users.CurrentId).ToList();

            string[] Statu = new string[] { "15" };
            if (site.M_FILE.FileRead == true && Statu.Contains(site.M_FILE.T_STATUS.StatuCode.ToString()))
            {
                site.M_FILE.FileRead = false;
                db.SaveChanges();
            }

            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "1")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditFiles(int id, string button, T_FILES_R File)
        {
            T_FILES files = db.T_FILES.Where(t => t.Id == id && t.WorkingUserId == _Users.CurrentId && t.T_STATUS.StatuCode == 15).FirstOrDefault();
            if (button == "record")
            {
                files.FileWaiting = true;
                files.DoctorDescription = File.DoctorDescription;
            }
            else
            {
                T_USER priceProtocol = db.T_USER.Where(t => t.Id == _Users.CurrentId).FirstOrDefault();
                files.FileWaiting = false;
                files.DoctorDescription = File.DoctorDescription + "\n\r----------------------------------------------------------------------\n\r";
                files.Status = 5;
                files.FileRead = true;
                files.Price2 = _Common.PriceProtocolCalc(priceProtocol.T_PRICEPROTOCOL.CalculationTypeId, priceProtocol.T_PRICEPROTOCOL.UnitPrice, files.DemandAmount);
                files.UpdateDate = DateTime.Now;
            }

            db.SaveChanges();

            return Redirect("/Expert/Dashboard");
        }

        [ErrorLog]
        [Authorize(Roles = "1")]
        public FileResult DownloadFile(long id)
        {
            T_DOCUMENTS doc = db.T_DOCUMENTS.Where(t => t.Id == id && t.T_FILES.WorkingUserId == _Users.CurrentId).FirstOrDefault();
            return File(doc.Contents, doc.ContentType, doc.ContentName);
        }

        [ErrorLog]
        [Authorize(Roles = "1")]
        public ActionResult FilesClosed()
        {
            BroadCast();
            site.M_FILES = db.T_FILES.Where(t => t.WorkingUserId == _Users.CurrentId && t.Status == 7).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "1")]
        public ActionResult FilesDetails(int id)
        {
            BroadCast();
            site.M_FILE = db.T_FILES.Where(t => t.Id == id && t.WorkingUserId == _Users.CurrentId).FirstOrDefault();
            site.M_SECTORLABEL = _Common.SectorLabel(site.M_FILE.FilesSectorId);
            site.M_STATUS = db.T_STATUS.ToList();
            site.M_DOCUMENTS = db.T_DOCUMENTS.Where(t => t.FileId == id && t.T_FILES.WorkingUserId == _Users.CurrentId).ToList();

            return View(site);
        }

        #endregion

        #region Profile

        [ErrorLog]
        [Authorize(Roles = "1")]
        public ActionResult EditProfile()
        {
            BroadCast();
            site.M_USER = db.T_USER.Where(t => t.Id == _Users.CurrentId && t.Status == 1).FirstOrDefault();
            site.M_CUSTOMERS = db.T_CUSTOMER.Where(t => t.Status == 1).ToList();
            site.M_PRICEPROTOCOLS = db.T_PRICEPROTOCOL.Where(t => t.Status == 1).ToList();
            site.M_BRANCHS = db.T_BRANCH.Where(t => t.BranchSectorId == _Users.CurrentSectorId && t.Status == 1).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "1")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditProfile(T_USER User)
        {
            T_USER UsrControl = db.T_USER.Where(t => t.Phone == User.Phone && t.Id != _Users.CurrentId && t.Status == 1).FirstOrDefault();

            if (UsrControl != null)
            {
                ViewBag.ErrorMessage = "";
                if (UsrControl.Email == User.Email)
                {
                    ViewBag.ErrorMessage += UsrControl.Email + " mail adresi sistemde mevcut. <br>";
                }
                if (UsrControl.Phone == User.Phone)
                {
                    ViewBag.ErrorMessage += UsrControl.Phone + " telefon numarası sistemde mevcut. <br>";
                }

                site.M_USER = User;
                site.M_CUSTOMERS = db.T_CUSTOMER.Where(t => t.Status == 1).ToList();
                site.M_PRICEPROTOCOLS = db.T_PRICEPROTOCOL.Where(t => t.Status == 1).ToList();
                site.M_BRANCHS = db.T_BRANCH.Where(t => t.BranchSectorId == _Users.CurrentSectorId && t.Status == 1).ToList();

                return View(site);
            }
            else
            {
                T_USER Usr = db.T_USER.Where(t => t.Id == _Users.CurrentId && t.Status == 1).FirstOrDefault();
                Usr.Name = User.Name;
                Usr.Gender = User.Gender;
                Usr.BirthDate = User.BirthDate;
                Usr.Phone = User.Phone;
                //Usr.PriceProtocolUserId = User.PriceProtocolUserId;
                //Usr.BranchUserId = User.BranchUserId;
                Usr.UpdateDate = DateTime.Now;
                db.SaveChanges();

                return Redirect("/Expert/EditProfile");
            }
        }

        [ErrorLog]
        [Authorize(Roles = "1")]
        [HttpPost]
        public JsonResult BlockedUser()
        {
            string status_header = "";
            T_USER Blk = db.T_USER.Where(t => t.Id == _Users.CurrentId && t.Status == 1).FirstOrDefault();
            if (Blk.Blocked == true)
            {
                Blk.Blocked = false;
                status_header = "<span>Uygun</span>";
            }
            else
            {
                Blk.Blocked = true;
                status_header = "<span class='color-red'>Uygun Değil</span>";
            }

            db.SaveChanges();

            return Json(status_header);
        }

        #endregion

        #region ChangePassword

        [ErrorLog]
        [Authorize(Roles = "1")]
        public ActionResult ChangePassword()
        {
            BroadCast();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "1")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword(string OldPassword, string NewPassword, string ConfirmNewPassword)
        {
            T_USER User = db.T_USER.Where(t => t.Id == _Users.CurrentId && t.Status == 1).FirstOrDefault();

            if (_Common.GetHashString(OldPassword) == User.Password)
            {

                if (NewPassword == ConfirmNewPassword)
                {
                    ViewBag.ErrorMessage = "Yeni şifreleriniz birbiriyle uyumsuz!";
                }
                else
                {
                    ViewBag.ErrorMessage = "Şifreniz değiştirildi ve mail adresinize gönderildi.";
                    string passwordhash = _Common.GetHashString(NewPassword);
                    User.Password = passwordhash;
                    User.PasswordCount = 3;
                    User.PasswordSetDate = DateTime.Now;
                    db.SaveChanges();
                    _Common.SendMail(User.Email, "İkinci göz sistemi kullanıcı şifreniz yenilendi", "Merhaba " + User.Name + "<br><br>İkinci göz sistemi için şifreniz : " + NewPassword);

                }
            }
            else
            {
                ViewBag.ErrorMessage = "Eski şifreniz hatalı!";
            }

            return View(site);
        }

        #endregion

        #region Statistics

        [ErrorLog]
        [Authorize(Roles = "1")]
        public ActionResult UserStatistics()
        {
            site.M_USER_STAT = db.T_USER_STATS.Where(t => t.UserId == _Users.CurrentId).FirstOrDefault();
            if (site.M_USER_STAT != null && site.M_USER_STAT.ClosedFileCount > 0)
                site.StatsRound = site.M_USER_STAT.TotalRating / site.M_USER_STAT.ClosedFileCount;
            else
                site.StatsRound = 0;

            return View(site);
        }

        #endregion

        #region Reports

        [ErrorLog]
        [Authorize(Roles = "1")]
        public ActionResult ProgressPaymentReport()
        {
            BroadCast();
            string month1 = "";
            string currency1 = "";
            string month2 = "";
            string currency2 = "";
            decimal cumulated = 0;
            site.M_FILES = db.T_FILES.Where(t => t.WorkingUserId == _Users.CurrentId && t.Status == 7 && t.IsCancel == false).OrderByDescending(t => t.UpdateDate).ToList();
            site.ProcessPayment = site.M_FILES.Where(s => s.IsFree == false && s.IsCancel == false).Sum(t => t.Price2);
            site.ClosedFiles = site.M_FILES.Count();
            site.M_USER_STAT = db.T_USER_STATS.Where(t => t.UserId == _Users.CurrentId).FirstOrDefault();
            site.Rating = (site.M_USER_STAT.ClosedFileCount == 0 ? 0 : site.M_USER_STAT.TotalRating / site.M_USER_STAT.ClosedFileCount);

            var data = db.T_FILES.Where(t => t.WorkingUserId == _Users.CurrentId && t.Status == 7 && t.IsCancel == false).Select(k => new { k.UpdateDate.Value.Year, k.UpdateDate.Value.Month, k.Price2, k.IsFree }).GroupBy(x => new { x.Year, x.Month, x.IsFree }, (key, group) => new
            {
                yr = key.Year,
                mnth = key.Month,
                tCharge = group.Sum(k => k.Price2),
                IsFree = key.IsFree
            }).ToList();

            site.M_REPORT_PROGRESS = _Common.ToDataTable(data);
            site.M_REPORT_CUMULATED = _Common.ToDataTable(data);

            for (int i = 0; i < site.M_REPORT_PROGRESS.Rows.Count; i++)
            {
                month1 += "'" + _Common.GetMonth(site.M_REPORT_PROGRESS.Rows[i][1].ToString()) + "'" + ",";
                if ((bool)site.M_REPORT_PROGRESS.Rows[i][3] == false)
                {
                    currency1 += site.M_REPORT_PROGRESS.Rows[i][2].ToString().Replace(',', '.') + ",";
                    cumulated += Convert.ToDecimal(site.M_REPORT_CUMULATED.Rows[i][2]);
                }
                else
                {
                    currency1 += "0,";
                    cumulated += 0;
                }
                month2 += "'" + _Common.GetMonth(site.M_REPORT_CUMULATED.Rows[i][1].ToString()) + "'" + ",";
                currency2 += cumulated.ToString().Replace(',', '.') + ",";
            }

            site.month1 = month1.Length > 0 ? (month1.Remove(month1.Length - 1)) : "";
            site.currency1 = currency1.Length > 0 ? currency1.Remove(currency1.Length - 1) : "";

            site.month2 = month2.Length > 0 ? month2.Remove(month2.Length - 1) : "";
            site.currency2 = currency2.Length > 0 ? currency2.Remove(currency2.Length - 1) : "";

            return View(site);
        }

        #endregion

        [ErrorLog]
        [Authorize(Roles = "1")]
        public JsonResult getFileSearch(string id)
        {
            return Json(db.T_FILES.Where(t => t.WorkingUserId == _Users.CurrentId && t.Id.ToString().StartsWith(id)).OrderBy(o => o.Id).Select(s => new { Id = s.Id + "( " + s.T_STATUS.StatuName + " )", Link = "/Expert/FilesDetails/" + s.Id }), JsonRequestBehavior.AllowGet);
        }

        [ErrorLog]
        private void BroadCast()
        {
            Statu = new string[] { "15" };
            site.M_BROADCAST_FILES = db.T_FILES.Where(t => t.WorkingUserId == _Users.CurrentId && Statu.Contains(t.T_STATUS.StatuCode.ToString()) && t.FileRead == true).OrderByDescending(t => t.UpdateDate).ToList();
            site.Controller = "Expert";
            site.Blocked = db.T_USER.Where(t => t.Id == _Users.CurrentId && t.Status == 1).FirstOrDefault().Blocked;
        }

    }
}