﻿using IkinciGorus.Models;
using System;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace IkinciGorus.Controllers
{
    public class LoginController : Controller
    {
        ContextDb db = new ContextDb();
        _Model site = new _Model();

        [ErrorLog]
        [AllowAnonymous]
        public ActionResult Login(string returnUrl)
        {
            CreateCaptchaCode();
            return View(site);
        }

        [ErrorLog]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult Login(string email, string password, string captcha, string returnUrl)
        {
            bool isTest = true;

            if (Session["CaptchaCode"].ToString() == _Common.OnlyCaptchaNumber(captcha) || isTest)
            {
                email = _Common.OnlyLoginString(email.ToLower());
                site.M_USER_LOGIN = db.T_USER.Where(u => u.Email.ToLower() == email && u.Status == 1).FirstOrDefault();

                if (site.M_USER_LOGIN != null)
                {
                    password = _Common.GetHashString(password); 
                    site.M_USER_PASSWORD = db.T_USER.Where(u => u.Email.ToLower() == email /*&& u.Password == password */ && u.Status == 1).FirstOrDefault();
                    if (site.M_USER_PASSWORD != null)
                    {
                        if (site.M_USER_PASSWORD.PasswordCount > 0 || isTest)
                        {
                            FormsAuthenticationTicket ticket = new FormsAuthenticationTicket(
                            1,
                            site.M_USER_PASSWORD.UserGuid + "~" + site.M_USER_PASSWORD.UserTitleId + "~" + site.M_USER_PASSWORD.Name + "~" + site.M_USER_PASSWORD.Email + "~" + site.M_USER_PASSWORD.Id + "~" + site.M_USER_PASSWORD.Gender + "~" + site.M_USER_PASSWORD.Manage + "~" + (site.M_USER_PASSWORD.T_CUSTOMER.Id > 0 ? site.M_USER_PASSWORD.T_CUSTOMER.Id : 0) + "~" + site.M_USER_PASSWORD.UserSectorId + "~" + site.M_USER_PASSWORD.CustomerOwner,
                            DateTime.Now,
                            DateTime.Now.AddHours(4),
                            true,
                            site.M_USER_PASSWORD.UserTitleId.ToString(),
                            FormsAuthentication.FormsCookiePath);

                            string hash = FormsAuthentication.Encrypt(ticket);
                            HttpCookie cookie = new HttpCookie(FormsAuthentication.FormsCookieName, hash);

                            Response.Cookies.Add(cookie);

                            site.M_USER_PASSWORD.PasswordCount = 3;
                            site.M_USER_PASSWORD.UpdateDate = DateTime.Now;
                            db.SaveChanges();
                            return Redirect(_Common.GetUrl(site.M_USER_PASSWORD.UserTitleId, returnUrl));
                        }
                        else
                        {
                            ViewBag.message = "<div class=\"alert alert-danger\" role=\"alert\"><div class=\"iq-alert-icon\"><i class=\"ri-information-line\"></i></div><div class=\"iq-alert-text\">Kullanıcınız kilitli, lütfen <a href=\"/ForgotPassword\">şifremi unuttum / şifre al</a> diyerek şifrenizi yenileyiniz.</div></div>";
                        }
                    }
                    site.M_USER_LOGIN.PasswordCount -= 1;
                    site.M_USER_LOGIN.UpdateDate = DateTime.Now;
                    db.SaveChanges();
                    ViewBag.message = "<div class=\"alert alert-danger\" role=\"alert\"><div class=\"iq-alert-icon\"><i class=\"ri-information-line\"></i></div><div class=\"iq-alert-text\">Şifreniz hatalıdır.  " + (site.M_USER_LOGIN.PasswordCount) + " denemeniz kalmıştır.</div></div>";

                    CreateCaptchaCode();

                    return View();
                }

                ViewBag.message = "<div class=\"alert alert-danger\" role=\"alert\"><div class=\"iq-alert-icon\"><i class=\"ri-information-line\"></i></div><div class=\"iq-alert-text\">Bilgileriniz hatalı !</div></div>";
            }
            else
            {
                ViewBag.message = "<div class=\"alert alert-danger\" role=\"alert\"><div class=\"iq-alert-icon\"><i class=\"ri-information-line\"></i></div><div class=\"iq-alert-text\">Güvenli kodu hatalı !</div></div>";

            }

            CreateCaptchaCode();

            return View();
        }

        [ErrorLog]
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            CreateCaptchaCode();
            return View();
        }

        [ErrorLog]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult ForgotPassword(string email, string captcha)
        {
            if (Session["CaptchaCode"].ToString() == captcha)
            {
                T_USER forgotUser = db.T_USER.Where(u => u.Email.ToLower() == email.ToLower() && u.Status == 1).FirstOrDefault();
                if (forgotUser == null)
                {
                    ViewBag.message = "<div class=\"alert alert-danger\" role=\"alert\"><div class=\"iq-alert-icon\"><i class=\"ri-information-line\"></i></div><div class=\"iq-alert-text\">E-mail adresiniz sistemde bulunamadı!</div></div>";
                }
                else
                {
                    string password = _Common.CreatePassword(6);
                    string passwordhash = _Common.GetHashString(password);
                    forgotUser.Password = passwordhash;
                    forgotUser.PasswordCount = 3;
                    forgotUser.PasswordSetDate = DateTime.Now;
                    db.SaveChanges();
                    _Common.SendMail(forgotUser.Email, "İkinci Göz sistemi kullanıcı şifreniz", "Merhaba " + forgotUser.Name + "<br><br>İkinci göz sistemi için<br><br>Kullanıcı Adınız : " + forgotUser.Email + "<br>Şifreniz : " + password);
                    ViewBag.message = "<div class=\"alert alert-success\" role=\"alert\"><div class=\"iq-alert-icon\"><i class=\"ri-information-line\"></i></div><div class=\"iq-alert-text\">Şifreniz E-mail adresinize gönderildi.</div></div>";
                }
            }
            else
            {
                ViewBag.message = "<div class=\"alert alert-danger\" role=\"alert\"><div class=\"iq-alert-icon\"><i class=\"ri-information-line\"></i></div><div class=\"iq-alert-text\">Güvenli kodu hatalı !</div></div>";
            }

            CreateCaptchaCode();

            return View();
        }

        [ErrorLog]
        [Authorize]
        public ActionResult SignOut()
        {
            FormsAuthentication.SignOut();
            return Redirect("/Login");
        }

        [ErrorLog]
        [AllowAnonymous]
        public ActionResult SignUp()
        {
            site.Signup = 0;
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Hide == false && t.Active == true).ToList();
            site.M_COMMUNICATIONCHOICE = _Common.CommunicationChoiceList();
            return View(site);
        }

        [ErrorLog]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public ActionResult SignUp(T_NEW_USER_REQUEST user, string[] Sector)
        {
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();

            T_NEW_USER_REQUEST Usr = new T_NEW_USER_REQUEST();
            Usr.Name = user.Name;
            Usr.Phone = user.Phone;
            Usr.Email = user.Email;
            Usr.CommunicationChoice = user.CommunicationChoice;
            int x = 2;
            int y;
            foreach (var item in site.M_SECTORS)
            {
                y = Array.IndexOf(Sector, Convert.ToString(x));
                if (y > -1)
                {
                    Usr.NewUserSector += x;
                    x++;
                }
                else
                {
                    Usr.NewUserSector += 0;
                    x++;
                }
            }
            Usr.Statu = 1;
            Usr.UserRead = false;
            Usr.CreateDate = DateTime.Now;
            db.T_NEW_USER_REQUEST.Add(Usr);
            db.SaveChanges();

            _Common.SendMail(user.Email, "İkinci göz sistemi kullanıcı talebi", "Merhaba " + user.Name + "<br><br>İkinci göz sistemi için talebiniz alınmıştır. Sizi " + _Common.GetCommunicationChoice(user.CommunicationChoice) + " aracılığı ile bilgilendiricez.<br><br>İyi günler dileriz");

            site.Signup = 1;

            return View(site);
        }

        [ErrorLog]
        [AllowAnonymous]
        public ActionResult Error()
        {
            return View(site);
        }

        [ErrorLog]
        [AllowAnonymous]
        public ActionResult PrivacyPolicy()
        {
            return View(site);
        }

        [ErrorLog]
        [AllowAnonymous]
        public ActionResult TermofUse()
        {
            return View(site);
        }

        [ErrorLog]
        public string GetCaptchaCode()
        {
            return GetCaptcha();
        }

        [ErrorLog]
        private string GetCaptcha()
        {
            CaptchaImage ci = new CaptchaImage(Session["CaptchaCode"].ToString(), 200, 41, "Arial");
            MemoryStream ms = new MemoryStream();
            ci.Image.Save(ms, ImageFormat.Jpeg);
            ci.Dispose();
            byte[] imageBytes = ms.ToArray();
            //FileContentResult img = File(ms.ToArray(), "image/gif");
            string base64String = Convert.ToBase64String(imageBytes);
            return base64String; //File(img.FileContents, img.ContentType);
        }

        [ErrorLog]
        public FileContentResult CreateCaptchaCode()
        {
            return CreateCaptcha(GenerateRandomCode());
        }

        [ErrorLog]
        private FileContentResult CreateCaptcha(string generate)
        {
            Session["CaptchaCode"] = generate;
            CaptchaImage ci = new CaptchaImage(Session["CaptchaCode"].ToString(), 200, 41, "Arial");
            MemoryStream ms = new MemoryStream();
            ci.Image.Save(ms, ImageFormat.Jpeg);
            ci.Dispose();
            FileContentResult img = File(ms.ToArray(), "image/gif");
            return File(img.FileContents, img.ContentType);
        }

        [ErrorLog]
        private string GenerateRandomCode()
        {
            Random random = new Random();
            string s = "";
            for (int i = 0; i < 6; i++)
                s = String.Concat(s, random.Next(10).ToString());
            return s;
        }
    }
}