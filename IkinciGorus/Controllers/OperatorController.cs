﻿using IkinciGorus.Enums;
using IkinciGorus.Models;
using iTextSharp.text.pdf.qrcode;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Configuration;
using System.Web.Mvc;

namespace IkinciGorus.Controllers
{
    public class OperatorController : Controller
    {
        _Model site = new _Model();
        ContextDb db = new ContextDb();
        string[] Statu = { "" };

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult Dashboard()
        {
            BroadCast();
            if (_Users.CurrentManage == false && _Users.CurrentCustomerOwner == true)
            {
                Response.Redirect("/Stakeholders/Index");
            }

            site.M_STATUS = db.T_STATUS.ToList();
            site.M_FILES = db.T_FILES.ToList();
            return View(site);
        }

        #region Files
        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult Files(string id)
        {
            BroadCast();

            if (id == null)
                site.M_FILES = db.T_FILES.Where(t => t.T_STATUS.StatuCode < 99).ToList();
            else
            {
                int Id = Int32.Parse(id);
                site.M_FILES = db.T_FILES.Where(t => t.T_STATUS.StatuCode < 99 && t.FilesSectorId == Id).ToList();
            }
            site.M_STATUS = db.T_STATUS.ToList();

            site.M_SECTORS = db.T_SECTOR.Where(t => t.Hide == false && t.Active == true).ToList();
            site.activeBtn = id;

            return View(site);

        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult FileList()
        {
            BroadCast();
            site.M_FILES = db.T_FILES.ToList();
            site.M_STATUS = db.T_STATUS.ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult FileStatuChange(long id)
        {
            site.M_FILE = db.T_FILES.Where(t => t.Id == id).FirstOrDefault();
            site.M_STATUS = db.T_STATUS.ToList();
            site.M_USERS = db.T_USER.Where(t => t.BranchUserId == site.M_FILE.BranchFileId && t.UserSectorId == site.M_FILE.FilesSectorId && t.Blocked == false && t.Status == 1).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        public JsonResult FileStatuChange(long id, int status, long workinguserid)
        {
            T_FILES file = db.T_FILES.Where(t => t.Id == id).FirstOrDefault();
            file.Status = status;
            file.WorkingUserId = workinguserid;

            // imaden uzman degisimi yada atamasi yapıldigi icin price2 yeniden hesaplansin
            T_USER expert = db.T_USER.Where(t => t.Id == workinguserid).FirstOrDefault();

            if (expert != null && expert.PriceProtocolUserId > 0)
            {
                T_PRICEPROTOCOL priceProtocol = db.T_PRICEPROTOCOL.Where(t => t.Id == expert.PriceProtocolUserId).FirstOrDefault();
                file.Price2 = _Common.PriceProtocolCalc(priceProtocol.CalculationTypeId, priceProtocol.UnitPrice, file.DemandAmount);
            }

            file.RollBackFile = false;
            file.UpdateDate = DateTime.Now;
            db.SaveChanges();

            string statuName = db.T_FILES.Where(t => t.Id == id).FirstOrDefault().T_STATUS.StatuName;
            return Json(new { idx = id, statu = statuName });
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult FilesClosed()
        {
            BroadCast();
            site.M_FILES = db.T_FILES.Where(t => t.T_STATUS.StatuCode == 99).ToList();
            site.M_STATUS = db.T_STATUS.ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult FilesDetails(long id)
        {
            BroadCast();
            site.M_FILE = db.T_FILES.Where(t => t.Id == id).FirstOrDefault();
            site.M_DOCUMENTS = db.T_DOCUMENTS.Where(t => t.FileId == id).ToList();
            site.M_STATUS = db.T_STATUS.ToList();
            site.M_MATERIALS = db.T_MATERIAL.ToList();

            site.M_SECTORLABEL = _Common.SectorLabel(site.M_FILE.FilesSectorId);
            site.SectorId = site.M_FILE.FilesSectorId;

            site.M_CUSTOMER = db.T_CUSTOMER.Where(t => t.Id == site.M_FILE.CreaterCustomerId).FirstOrDefault();

            site.link = (site.M_FILE.Status == 7 ? "filesclosed" : "fileslist");

            string[] Statu = new string[] { "25" };
            if (site.M_FILE.FileRead == true && Statu.Contains(site.M_FILE.T_STATUS.StatuCode.ToString()))
            {
                site.M_FILE.FileRead = false;
                db.SaveChanges();
            }

            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult FilesDetails(int id, string button, T_FILES_R File)
        {
            if (button == "record")
            {
                T_FILES files = db.T_FILES.Where(t => t.Id == id && t.T_STATUS.StatuCode == 25).FirstOrDefault();

                files.FileWaiting = true;
                files.DoctorDescription = File.DoctorDescription;
            }
            else if (button == "rollbackfile")
            {
                T_FILES files = db.T_FILES.Where(t => t.Id == id).FirstOrDefault();

                if (files.RollBackFile == false)
                    files.RollBackFile = true;
                else
                    files.RollBackFile = false;

                files.UpdateDate = DateTime.Now;
                db.SaveChanges();
            }
            else if (button == "iscancel")
            {
                T_FILES files = db.T_FILES.Where(t => t.Id == id).FirstOrDefault();

                files.IsCancel = true;
                files.RollBackFile = false;
                files.Status = 7;
                files.UpdateDate = DateTime.Now;

                db.SaveChanges();
            }
            else
            {
                T_FILES files = db.T_FILES.Where(t => t.Id == id && t.T_STATUS.StatuCode == 25).FirstOrDefault();

                files.FileWaiting = false;
                files.DoctorDescription = File.DoctorDescription;
                files.Status = 6;
                files.FileRead = true;
                files.UpdateDate = DateTime.Now;
            }

            db.SaveChanges();

            return Redirect("/Operator/Files");
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public FileResult DownloadFile(long id)
        {
            T_DOCUMENTS doc = db.T_DOCUMENTS.Where(t => t.Id == id).FirstOrDefault();
            return File(doc.Contents, doc.ContentType, doc.ContentName);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult CreatePdf(long id)
        {
            site.M_FILE = db.T_FILES.Where(t => t.Id == id).FirstOrDefault();
            site.M_SECTORLABEL = _Common.SectorLabel(site.M_FILE.FilesSectorId);
            _CreatePdf.GenerateInvoice(site.M_FILE, site.M_SECTORLABEL);
            return View();
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        public JsonResult FileIsFree(long id)
        {
            string alert;

            T_FILES file = db.T_FILES.Where(t => t.Id == id).FirstOrDefault();

            if (file.IsFree == true)
            {
                file.IsFree = false;
                alert = "Dosya ücretli hale getirilmiştir.";
            }
            else
            {
                file.IsFree = true;
                alert = "Dosya ücretsiz hale getirilmiştir.";
            }

            //file.UpdateDate = DateTime.Now;
            db.SaveChanges();

            return Json(new { alert = alert, free = file.IsFree });
        }

        #endregion

        #region Customer

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult Customers()
        {
            BroadCast();
            site.M_CUSTOMERS = db.T_CUSTOMER.ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult NewCustomers()
        {
            BroadCast();
            site.M_CUSTOMER = new T_CUSTOMER();
            site.M_CITYS = db.T_CITY.ToList();
            site.M_PRICEPROTOCOLS = db.T_PRICEPROTOCOL.Where(t => t.Status == 1).ToList();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewCustomers(T_CUSTOMER Customer, string[] Sector)
        {
            T_CUSTOMER CustControl = db.T_CUSTOMER.Where(t => (t.Phone == Customer.Phone || t.Email == Customer.Email) && t.Status == 1).FirstOrDefault();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();

            if (CustControl != null)
            {
                ViewBag.ErrorMessage = "";
                if (CustControl.Email == Customer.Email)
                {
                    ViewBag.ErrorMessage += CustControl.Email + " mail adresi sistemde mevcut. <br>";
                }
                if (CustControl.Phone == Customer.Phone)
                {
                    ViewBag.ErrorMessage += CustControl.Phone + " telefon numarası sistemde mevcut. <br>";
                }

                site.M_CUSTOMER = Customer;
                site.M_CITYS = db.T_CITY.ToList();
                site.M_PRICEPROTOCOLS = db.T_PRICEPROTOCOL.Where(t => t.Status == 1).ToList();
                BroadCast();
                return View(site);
            }
            else
            {
                T_CUSTOMER Cust = new T_CUSTOMER();
                Cust.CustomerGuid = Guid.NewGuid().ToString();
                int x = 1;
                int y;
                foreach (var item in site.M_SECTORS)
                {
                    y = Array.IndexOf(Sector, Convert.ToString(x));
                    if (y > -1)
                    {
                        Cust.CustomerSector += x;
                        x++;
                    }
                    else
                    {
                        Cust.CustomerSector += 0;
                        x++;
                    }
                }
                Cust.TitleName = Customer.TitleName;
                Cust.Vkn = Customer.Vkn;
                Cust.Adress = Customer.Adress;
                Cust.City = Customer.City;
                Cust.Phone = Customer.Phone;
                Cust.Email = Customer.Email;
                Cust.Status = Customer.Status;
                Cust.PriceProtocolHealthId = Customer.PriceProtocolHealthId;
                Cust.PriceProtocolLawId = Customer.PriceProtocolLawId;
                Cust.PriceProtocolInsuranceId = Customer.PriceProtocolInsuranceId;
                Cust.DiscountRate = 7;
                Cust.ValorDay = Convert.ToInt16(WebConfigurationManager.AppSettings["jobRejectedDay"]);
                Cust.CreateDate = DateTime.Now;
                Cust.UpdateDate = DateTime.Now;
                db.T_CUSTOMER.Add(Cust);

                T_CUSTOMER_STATS cstats = new T_CUSTOMER_STATS();
                cstats.CustomerId = Cust.Id;
                cstats.ClosedFileCount = 0;
                cstats.TotalRating = 0;
                cstats.LastUpdateDate = DateTime.Now;
                db.T_CUSTOMER_STATS.Add(cstats);

                db.SaveChanges();

                return Redirect("/Operator/Customers");
            }
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult EditCustomers(int id)
        {
            BroadCast();
            site.M_CUSTOMER = db.T_CUSTOMER.Where(t => t.Id == id).FirstOrDefault();
            site.M_CITYS = db.T_CITY.ToList();
            site.M_PRICEPROTOCOLS = db.T_PRICEPROTOCOL.Where(t => t.Status == 1).ToList();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult ViewCustomer(int id)
        {
            BroadCast();
            site.M_CUSTOMER = db.T_CUSTOMER.Where(t => t.Id == id).FirstOrDefault();
            site.M_CITYS = db.T_CITY.ToList();
            site.M_PRICEPROTOCOLS = db.T_PRICEPROTOCOL.Where(t => t.Status == 1).ToList();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditCustomers(int id, T_CUSTOMER Customer, string[] Sector)
        {
            T_CUSTOMER CustControl = db.T_CUSTOMER.Where(t => (t.Phone == Customer.Phone || t.Email == Customer.Email) && t.Id != id && t.Status == 1).FirstOrDefault();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();

            if (CustControl != null)
            {
                ViewBag.ErrorMessage = "";
                if (CustControl.Email == Customer.Email)
                {
                    ViewBag.ErrorMessage += CustControl.Email + " mail adresi sistemde mevcut. <br>";
                }
                if (CustControl.Phone == Customer.Phone)
                {
                    ViewBag.ErrorMessage += CustControl.Phone + " telefon numarası sistemde mevcut. <br>";
                }

                site.M_CUSTOMER = Customer;
                site.M_CITYS = db.T_CITY.ToList();
                site.M_PRICEPROTOCOLS = db.T_PRICEPROTOCOL.Where(t => t.Status == 1).ToList();
                BroadCast();
                return View(site);
            }
            else
            {
                T_CUSTOMER Cust = db.T_CUSTOMER.Where(t => t.Id == id).FirstOrDefault();
                Cust.CustomerSector = "";
                int x = 1;
                int y;
                foreach (var item in site.M_SECTORS)
                {
                    y = Array.IndexOf(Sector, Convert.ToString(x));
                    if (y > -1)
                    {
                        Cust.CustomerSector += x;
                        x++;
                    }
                    else
                    {
                        Cust.CustomerSector += 0;
                        x++;
                    }
                }
                Cust.TitleName = Customer.TitleName;
                Cust.Vkn = Customer.Vkn;
                Cust.Adress = Customer.Adress;
                Cust.City = Customer.City;
                Cust.Phone = Customer.Phone;
                Cust.Email = Customer.Email;
                Cust.Status = Customer.Status;
                Cust.PriceProtocolHealthId = Customer.PriceProtocolHealthId;
                Cust.PriceProtocolLawId = Customer.PriceProtocolLawId;
                Cust.PriceProtocolInsuranceId = Customer.PriceProtocolInsuranceId;
                Cust.UpdateDate = DateTime.Now;
                db.SaveChanges();

                return Redirect("/Operator/Customers");
            }
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult DeleteCustomers(int id)
        {
            T_CUSTOMER Cust = db.T_CUSTOMER.Where(t => t.Id == id).FirstOrDefault();
            Cust.Status = 0;
            db.SaveChanges();
            return Redirect("/Operator/Customers");
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult CustomerOwner(int id)
        {
            BroadCast();
            site.M_CUSTOMER = db.T_CUSTOMER.Where(t => t.Id == id).FirstOrDefault();
            site.M_CUSTOMER_OWNERS = db.T_CUSTOMER_OWNER.Where(t => t.OwnerCustomerId == id).ToList();
            site.M_USERS = db.T_USER.Where(t => t.CustomerOwner == true).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CustomerOwner(long?[] id, long ownercustomerid, long[] owneruserid, string[] ownerrate, string button)
        {
            if (button == "add")
            {

                long? idx = 0;

                for (int i = 0; i < owneruserid.Length; i++)
                {
                    idx = owneruserid[i];
                    T_CUSTOMER_OWNER co = db.T_CUSTOMER_OWNER.Where(t => t.OwnerUserId == idx && t.OwnerCustomerId == ownercustomerid).FirstOrDefault();

                    if (co == null)
                    {
                        T_CUSTOMER_OWNER nco = new T_CUSTOMER_OWNER();
                        nco.OwnerCustomerId = ownercustomerid;
                        nco.OwnerUserId = owneruserid[i];
                        nco.OwnerRate = Convert.ToDouble(ownerrate[i].Replace(".", ","));
                        db.T_CUSTOMER_OWNER.Add(nco);
                    }
                    else
                    {
                        co.OwnerRate = Convert.ToDouble(ownerrate[i]);
                    }
                    db.SaveChanges();
                }
            }
            else
            {
                List<T_CUSTOMER_OWNER> co = db.T_CUSTOMER_OWNER.Where(t => t.OwnerCustomerId == ownercustomerid).ToList();
                db.T_CUSTOMER_OWNER.RemoveRange(co);
                db.SaveChanges();
            }

            return Redirect("/Operator/Customers");
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        public JsonResult DeleteCustomerOwner(long id)
        {
            T_CUSTOMER_OWNER co = db.T_CUSTOMER_OWNER.Where(t => t.Id == id).FirstOrDefault();
            if (co != null)
            {
                db.T_CUSTOMER_OWNER.Remove(co);
                db.SaveChanges();
            }
            return Json("Paydaş silindi");
        }

        #endregion

        #region Users

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult Users(string id)
        {
            BroadCast();

            site.M_USERS = (id == null ? db.T_USER.ToList() : db.T_USER.Where(t => t.T_SECTOR.TagName == id).ToList());
            site.activeBtn = id;
            site.M_SECTORS = db.T_SECTOR.ToList();

            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult NewUsers()
        {
            BroadCast();
            site.M_USER = new T_USER();
            site.M_CUSTOMERS = db.T_CUSTOMER.Where(t => t.Status == 1).ToList();
            //site.M_PRICEPROTOCOLS = db.T_PRICEPROTOCOL.Where(t => t.Status == 1).ToList();
            //site.M_BRANCHS = db.T_BRANCH.Where(t => t.Status == 1).ToList();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
            site.M_TITLES = db.T_TITLE.Where(t => t.Active == true).ToList();
            //site.M_TITLELISTS = _Common.TitleList();
            //site.M_SECTORLISTS = _Common.SectorList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewUsers(T_USER User)
        {
            T_USER UsrControl = db.T_USER.Where(t => (t.Phone == User.Phone || t.Email == User.Email) && t.Status == 1).FirstOrDefault();

            if (UsrControl != null)
            {
                ViewBag.ErrorMessage = "";
                if (UsrControl.Email == User.Email)
                {
                    ViewBag.ErrorMessage += UsrControl.Email + " mail adresi sistemde mevcut. <br>";
                }
                if (UsrControl.Phone == User.Phone)
                {
                    ViewBag.ErrorMessage += UsrControl.Phone + " telefon numarası sistemde mevcut. <br>";
                }

                site.M_USER = User;
                site.M_CUSTOMERS = db.T_CUSTOMER.Where(t => t.Status == 1).ToList();
                site.M_PRICEPROTOCOLS = db.T_PRICEPROTOCOL.Where(t => t.Status == 1).ToList();
                site.M_BRANCHS = db.T_BRANCH.Where(t => t.Status == 1).ToList();
                site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
                site.M_TITLES = db.T_TITLE.Where(t => t.Active == true).ToList();
                BroadCast();
                return View(site);
            }
            else
            {
                T_USER Usr = new T_USER();
                Usr.UserGuid = Guid.NewGuid().ToString();
                Usr.Name = User.Name;
                Usr.UserSectorId = User.UserSectorId;
                Usr.UserTitleId = User.UserTitleId;
                Usr.Gender = User.Gender;
                Usr.BirthDate = User.BirthDate;
                Usr.Phone = User.Phone;
                Usr.Email = User.Email;
                Usr.Status = User.Status;
                Usr.CustomerId = User.CustomerId;
                Usr.PriceProtocolUserId = User.PriceProtocolUserId;
                Usr.BranchUserId = User.BranchUserId;
                Usr.Manage = User.Manage;
                if (User.Manage == null)
                    Usr.Manage = false;
                Usr.Blocked = false;
                Usr.CustomerOwner = User.CustomerOwner;
                if (User.CustomerOwner == null)
                    Usr.CustomerOwner = false;
                Usr.CreateDate = DateTime.Now;
                Usr.UpdateDate = DateTime.Now;

                string password = _Common.CreatePassword(6);
                string passwordhash = _Common.GetHashString(password);
                Usr.Password = passwordhash;
                Usr.PasswordCount = 3;
                Usr.PasswordSetDate = DateTime.Now;
                db.T_USER.Add(Usr);

                T_USER_STATS ustats = new T_USER_STATS();
                ustats.UserId = Usr.Id;
                ustats.ClosedFileCount = 0;
                ustats.TotalRating = 0;
                ustats.LastUpdateDate = DateTime.Now;
                db.T_USER_STATS.Add(ustats);

                db.SaveChanges();

                _Common.SendMail(User.Email, "İkinci göz sistemi kullanıcı şifreniz", "Merhaba " + User.Name + "<br><br>İkinci göz sistemi için şifreniz : " + password);

                return Redirect("/Operator/Users");
            }
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult EditUsers(int id)
        {
            BroadCast();
            site.M_USER = db.T_USER.Where(t => t.Id == id).FirstOrDefault();
            site.M_CUSTOMERS = db.T_CUSTOMER.Where(t => t.Status == 1).ToList();
            site.M_PRICEPROTOCOLS = db.T_PRICEPROTOCOL.Where(t => t.PriProtSectorId == site.M_USER.UserSectorId && t.Status == 1).ToList();
            site.M_BRANCHS = db.T_BRANCH.Where(t => t.BranchSectorId == site.M_USER.UserSectorId && t.Status == 1).ToList();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
            site.M_TITLES = db.T_TITLE.Where(t => t.Active == true).ToList();
            //site.M_TITLELISTS = _Common.TitleList();
            //site.M_SECTORLISTS = _Common.SectorList();+

            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult ViewUser(int id)
        {
            BroadCast();
            site.M_USER = db.T_USER.Where(t => t.Id == id).FirstOrDefault();
            site.M_CUSTOMERS = db.T_CUSTOMER.Where(t => t.Status == 1).ToList();
            site.M_PRICEPROTOCOLS = db.T_PRICEPROTOCOL.Where(t => t.Status == 1).ToList();
            site.M_BRANCHS = db.T_BRANCH.Where(t => t.Status == 1).ToList();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
            site.M_TITLES = db.T_TITLE.Where(t => t.Active == true).ToList();
            //site.M_TITLELISTS = _Common.TitleList();
            //site.M_SECTORLISTS = _Common.SectorList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditUsers(int id, T_USER User)
        {
            T_USER UsrControl = db.T_USER.Where(t => (t.Phone == User.Phone || t.Email == User.Email) && t.Id != id && t.Status == 1).FirstOrDefault();

            if (UsrControl != null)
            {
                ViewBag.ErrorMessage = "";
                if (UsrControl.Email == User.Email)
                {
                    ViewBag.ErrorMessage += UsrControl.Email + " mail adresi sistemde mevcut. <br>";
                }
                if (UsrControl.Phone == User.Phone)
                {
                    ViewBag.ErrorMessage += UsrControl.Phone + " telefon numarası sistemde mevcut. <br>";
                }

                site.M_USER = User;
                site.M_CUSTOMERS = db.T_CUSTOMER.Where(t => t.Status == 1).ToList();
                site.M_PRICEPROTOCOLS = db.T_PRICEPROTOCOL.Where(t => t.Status == 1).ToList();
                site.M_BRANCHS = db.T_BRANCH.Where(t => t.Status == 1).ToList();
                site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
                site.M_TITLES = db.T_TITLE.Where(t => t.Active == true).ToList();
                BroadCast();
                return View(site);
            }
            else
            {
                T_USER Usr = db.T_USER.Where(t => t.Id == id).FirstOrDefault();
                Usr.Name = User.Name;
                Usr.UserSectorId = User.UserSectorId;
                Usr.UserTitleId = User.UserTitleId;
                Usr.Gender = User.Gender;
                Usr.BirthDate = User.BirthDate;
                Usr.Phone = User.Phone;
                Usr.Email = User.Email;
                Usr.Status = User.Status;
                Usr.CustomerId = User.CustomerId;
                Usr.PriceProtocolUserId = User.PriceProtocolUserId;
                Usr.BranchUserId = User.BranchUserId;
                Usr.Manage = User.Manage;
                if (User.Manage == null)
                    Usr.Manage = false;
                Usr.CustomerOwner = User.CustomerOwner;
                if (User.CustomerOwner == null)
                    Usr.CustomerOwner = false;
                Usr.UpdateDate = DateTime.Now;
                db.SaveChanges();

                return Redirect("/Operator/Users");
            }
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult DeleteUsers(int id)
        {
            T_USER Usr = db.T_USER.Where(t => t.Id == id).FirstOrDefault();
            Usr.Status = 0;
            db.SaveChanges();
            return Redirect("/Operator/Users");
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public JsonResult UserPasswordSender(int id)
        {
            T_USER Usr = db.T_USER.Where(t => t.Id == id).FirstOrDefault();
            string password = _Common.CreatePassword(6);
            string passwordhash = _Common.GetHashString(password);
            Usr.Password = passwordhash;
            Usr.PasswordCount = 3;
            Usr.PasswordSetDate = DateTime.Now;
            db.SaveChanges();

            _Common.SendMail(Usr.Email, "İkinci göz sistemi kullanıcı şifreniz", "Merhaba " + Usr.Name + "<br><br>İkinci göz sistemi için şifreniz : " + password);

            return Json("Şifre gönderildi.", JsonRequestBehavior.AllowGet);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public JsonResult BlockedUser(int id)
        {
            T_USER Blk = db.T_USER.Where(t => t.Id == id).FirstOrDefault();
            if (Blk.Blocked == true)
                Blk.Blocked = false;
            else
                Blk.Blocked = true;

            db.SaveChanges();

            return Json(Blk.Blocked.ToString());
        }

        #endregion

        #region Hospital

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult Hospitals()
        {
            BroadCast();
            site.M_HOSPITALS = db.T_HOSPITAL.ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult NewHospitals()
        {
            BroadCast();
            site.M_HOSPITAL = new T_HOSPITAL();
            site.M_CITYS = db.T_CITY.ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewHospitals(T_HOSPITAL Hospital)
        {
            T_HOSPITAL HosControl = db.T_HOSPITAL.Where(t => (t.Phone == Hospital.Phone || t.Email == Hospital.Email) && t.Status == 1).FirstOrDefault();

            if (HosControl != null)
            {
                ViewBag.ErrorMessage = "";
                if (HosControl.Email == Hospital.Email)
                {
                    ViewBag.ErrorMessage += HosControl.Email + " mail adresi sistemde mevcut. <br>";
                }
                if (HosControl.Phone == Hospital.Phone)
                {
                    ViewBag.ErrorMessage += HosControl.Phone + " telefon numarası sistemde mevcut. <br>";
                }

                site.M_HOSPITAL = Hospital;
                site.M_CITYS = db.T_CITY.ToList();
                BroadCast();
                return View(site);
            }
            else
            {
                T_HOSPITAL Hosp = new T_HOSPITAL();
                Hosp.TitleName = Hospital.TitleName;
                Hosp.City = Hospital.City;
                Hosp.Phone = Hospital.Phone;
                Hosp.Email = Hospital.Email;
                Hosp.Status = Hospital.Status;
                Hosp.CreateDate = DateTime.Now;
                Hosp.UpdateDate = DateTime.Now;
                db.T_HOSPITAL.Add(Hosp);
                db.SaveChanges();

                return Redirect("/Operator/Hospitals");
            }
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult EditHospitals(int id)
        {
            BroadCast();
            site.M_HOSPITAL = db.T_HOSPITAL.Where(t => t.Id == id).FirstOrDefault();
            site.M_CITYS = db.T_CITY.ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult ViewHospital(int id)
        {
            BroadCast();
            site.M_HOSPITAL = db.T_HOSPITAL.Where(t => t.Id == id).FirstOrDefault();
            site.M_CITYS = db.T_CITY.ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditHospitals(int id, T_HOSPITAL Hospital)
        {
            T_HOSPITAL HosControl = db.T_HOSPITAL.Where(t => (t.Phone == Hospital.Phone || t.Email == Hospital.Email) && t.Id != id && t.Status == 1).FirstOrDefault();

            if (HosControl != null)
            {
                ViewBag.ErrorMessage = "";
                if (HosControl.Email == Hospital.Email)
                {
                    ViewBag.ErrorMessage += HosControl.Email + " mail adresi sistemde mevcut. <br>";
                }
                if (HosControl.Phone == Hospital.Phone)
                {
                    ViewBag.ErrorMessage += HosControl.Phone + " telefon numarası sistemde mevcut. <br>";
                }

                site.M_HOSPITAL = Hospital;
                site.M_CITYS = db.T_CITY.ToList();
                BroadCast();
                return View(site);
            }
            else
            {
                T_HOSPITAL Hosp = db.T_HOSPITAL.Where(t => t.Id == id).FirstOrDefault();
                Hosp.TitleName = Hospital.TitleName;
                Hosp.City = Hospital.City;
                Hosp.Phone = Hospital.Phone;
                Hosp.Email = Hospital.Email;
                Hosp.Status = Hospital.Status;
                Hosp.UpdateDate = DateTime.Now;
                db.SaveChanges();

                return Redirect("/Operator/Hospitals");
            }
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult DeleteHospitals(int id)
        {
            T_HOSPITAL Hosp = db.T_HOSPITAL.Where(t => t.Id == id).FirstOrDefault();
            Hosp.Status = 0;
            db.SaveChanges();
            return Redirect("/Operator/Hospitals");
        }

        #endregion

        #region Branch

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult Branchs()
        {
            BroadCast();
            site.M_BRANCHS = db.T_BRANCH.ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult NewBranchs()
        {
            BroadCast();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewBranchs(T_BRANCH Branch)
        {
            T_BRANCH Bran = new T_BRANCH();
            Bran.BranchSectorId = Branch.BranchSectorId;
            Bran.BranchName = Branch.BranchName;
            Bran.Status = Branch.Status;
            Bran.CreateDate = DateTime.Now;
            Bran.UpdateDate = DateTime.Now;
            db.T_BRANCH.Add(Bran);
            db.SaveChanges();

            return Redirect("/Operator/Branchs");
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult EditBranchs(int id)
        {
            BroadCast();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
            site.M_BRANCH = db.T_BRANCH.Where(t => t.Id == id).FirstOrDefault();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult ViewBranch(int id)
        {
            BroadCast();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
            site.M_BRANCH = db.T_BRANCH.Where(t => t.Id == id).FirstOrDefault();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditBranchs(int id, T_BRANCH Branch)
        {
            T_BRANCH Bran = db.T_BRANCH.Where(t => t.Id == id).FirstOrDefault();
            Bran.BranchSectorId = Branch.BranchSectorId;
            Bran.BranchName = Branch.BranchName;
            Bran.Status = Branch.Status;
            Bran.UpdateDate = DateTime.Now;
            db.SaveChanges();

            return Redirect("/Operator/Branchs");
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult DeleteBranchs(int id)
        {
            T_BRANCH Bran = db.T_BRANCH.Where(t => t.Id == id).FirstOrDefault();
            Bran.Status = 0;
            db.SaveChanges();
            return Redirect("/Operator/Branchs");
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        private IList<T_BRANCH> getListBranch(int id)
        {
            return db.T_BRANCH.Where(t => t.BranchSectorId == id && t.Status == 1).ToList();
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult getBranch(int id)
        {
            var branch = this.getListBranch(id);
            var branchdata = branch.Select(t => new SelectListItem()
            {
                Text = t.BranchName,
                Value = t.Id.ToString()
            });

            return Json(branchdata, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Material

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult Materials()
        {
            BroadCast();
            site.M_MATERIALS = db.T_MATERIAL.ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult NewMaterials()
        {
            BroadCast();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewMaterials(T_MATERIAL Material)
        {
            T_MATERIAL Mate = new T_MATERIAL();
            Mate.MaterialName = Material.MaterialName;
            Mate.UnitPrice = Material.UnitPrice;
            Mate.UbbUts = Material.UbbUts;
            Mate.Sut = Material.Sut;
            Mate.UseNumber = Material.UseNumber;
            Mate.ValidityDate = Material.ValidityDate;
            Mate.Status = Material.Status;
            Mate.CreateDate = DateTime.Now;
            Mate.UpdateDate = DateTime.Now;
            db.T_MATERIAL.Add(Mate);
            db.SaveChanges();

            return Redirect("/Operator/Materials");
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult EditMaterials(int id)
        {
            BroadCast();
            site.M_MATERIAL = db.T_MATERIAL.Where(t => t.Id == id).FirstOrDefault();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult ViewMaterial(int id)
        {
            BroadCast();
            site.M_MATERIAL = db.T_MATERIAL.Where(t => t.Id == id).FirstOrDefault();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditMaterials(int id, T_MATERIAL Material)
        {
            T_MATERIAL Mate = db.T_MATERIAL.Where(t => t.Id == id).FirstOrDefault();
            Mate.MaterialName = Material.MaterialName;
            Mate.UnitPrice = Material.UnitPrice;
            Mate.UbbUts = Material.UbbUts;
            Mate.Sut = Material.Sut;
            Mate.UseNumber = Material.UseNumber;
            Mate.ValidityDate = Material.ValidityDate;
            Mate.Status = Material.Status;
            Mate.UpdateDate = DateTime.Now;
            db.SaveChanges();

            return Redirect("/Operator/Materials");
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult DeleteMaterials(int id)
        {
            T_MATERIAL Mate = db.T_MATERIAL.Where(t => t.Id == id).FirstOrDefault();
            Mate.Status = 0;
            db.SaveChanges();
            return Redirect("/Operator/Materials");
        }

        #endregion

        #region PriceProtocol

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult PriceProtocols(string id)
        {
            BroadCast();
            site.M_PRICEPROTOCOLS = (id == null ? db.T_PRICEPROTOCOL.ToList() : db.T_PRICEPROTOCOL.Where(t => t.T_SECTOR.TagName == id).ToList());
            site.activeBtn = id;
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Hide == false && t.Active == true).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult NewPriceProtocols()
        {
            BroadCast();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult NewPriceProtocols(T_PRICEPROTOCOL PriceProtocol)
        {
            T_PRICEPROTOCOL PrPr = new T_PRICEPROTOCOL();
            PrPr.PriProtSectorId = PriceProtocol.PriProtSectorId;
            PrPr.ProtocolName = PriceProtocol.ProtocolName;
            PrPr.CalculationTypeId = PriceProtocol.CalculationTypeId;
            PrPr.UnitPrice = PriceProtocol.UnitPrice;
            PrPr.ValidityDate = PriceProtocol.ValidityDate;
            PrPr.Status = PriceProtocol.Status;
            PrPr.CreateDate = DateTime.Now;
            PrPr.UpdateDate = DateTime.Now;
            db.T_PRICEPROTOCOL.Add(PrPr);
            db.SaveChanges();

            return Redirect("/Operator/PriceProtocols");
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult EditPriceProtocols(int id)
        {
            BroadCast();
            site.M_PRICEPROTOCOL = db.T_PRICEPROTOCOL.Where(t => t.Id == id).FirstOrDefault();
            site.M_PRICEPROCCALCTYPES = db.T_PRICEPROCCALCTYPE.Where(t => t.CalcTypeSectorId == site.M_PRICEPROTOCOL.PriProtSectorId).ToList();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult ViewPriceProtocol(int id)
        {
            BroadCast();
            site.M_PRICEPROTOCOL = db.T_PRICEPROTOCOL.Where(t => t.Id == id).FirstOrDefault();
            site.M_PRICEPROCCALCTYPES = db.T_PRICEPROCCALCTYPE.Where(t => t.CalcTypeSectorId == site.M_PRICEPROTOCOL.PriProtSectorId).ToList();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult EditPriceProtocols(int id, T_PRICEPROTOCOL PriceProtocol)
        {
            T_PRICEPROTOCOL PrPr = db.T_PRICEPROTOCOL.Where(t => t.Id == id).FirstOrDefault();
            PrPr.PriProtSectorId = PriceProtocol.PriProtSectorId;
            PrPr.ProtocolName = PriceProtocol.ProtocolName;
            PrPr.CalculationTypeId = PriceProtocol.CalculationTypeId;
            PrPr.UnitPrice = PriceProtocol.UnitPrice;
            PrPr.ValidityDate = PriceProtocol.ValidityDate;
            PrPr.Status = PriceProtocol.Status;
            PrPr.UpdateDate = DateTime.Now;
            db.SaveChanges();

            return Redirect("/Operator/PriceProtocols");
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult DeletePriceProtocols(int id)
        {
            T_PRICEPROTOCOL PrPr = db.T_PRICEPROTOCOL.Where(t => t.Id == id).FirstOrDefault();
            PrPr.Status = 0;
            db.SaveChanges();
            return Redirect("/Operator/PriceProtocols");
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        private IList<T_PRICEPROTOCOL> getListPriceProtocol(int id)
        {
            return db.T_PRICEPROTOCOL.Where(t => t.PriProtSectorId == id && t.Status == 1).ToList();
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult getPriceProtocol(int id)
        {
            var priceprotocol = this.getListPriceProtocol(id);
            var priceprotocoldata = priceprotocol.Select(t => new SelectListItem()
            {
                Text = t.ProtocolName,
                Value = t.Id.ToString()
            });

            return Json(priceprotocoldata, JsonRequestBehavior.AllowGet);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        private IList<T_PRICEPROCCALCTYPE> getListProtocolCalcType(int id)
        {
            return site.M_PRICEPROCCALCTYPES = db.T_PRICEPROCCALCTYPE.Where(t => t.CalcTypeSectorId == id).ToList();
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [AcceptVerbs(HttpVerbs.Get)]
        public JsonResult getPriceProtocolCalcType(int id)
        {
            var priceprotocoltype = this.getListProtocolCalcType(id);
            var priceprotocoltypedata = priceprotocoltype.Select(t => new SelectListItem()
            {
                Text = t.CalcTypeName,
                Value = t.Id.ToString()
            });

            return Json(priceprotocoltypedata, JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region Yeni Müşteri Talebi

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult NewUserRequest()
        {
            BroadCast();
            site.M_NEW_USER_REQUESTS = db.T_NEW_USER_REQUEST.ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult ViewNewUserRequest(int id)
        {
            BroadCast();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
            site.M_SIGNSTATUS = _Common.StatuList();
            site.M_NEW_USER_REQUEST = db.T_NEW_USER_REQUEST.Where(t => t.Id == id).FirstOrDefault();
            site.M_NEW_USER_REQUEST.UserRead = true;
            db.SaveChanges();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ViewNewUserRequest(T_NEW_USER_REQUEST user, int id)
        {
            T_NEW_USER_REQUEST usr = db.T_NEW_USER_REQUEST.Where(t => t.Id == id).FirstOrDefault();
            usr.Statu = user.Statu;
            usr.OperationComment = user.OperationComment;
            db.SaveChanges();
            return Redirect("/Operator/NewUserRequest");
        }

        #endregion

        #region Statistics

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult UserStatistics()
        {
            BroadCast();
            site.M_SECTORS = db.T_SECTOR.Where(t => t.Active == true).ToList();
            site.M_USER_STATS = db.T_USER_STATS.Where(t => t.T_USER.UserTitleId == 1).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult CustomerStatistics()
        {
            BroadCast();
            site.M_CUSTOMER_STATS = db.T_CUSTOMER_STATS.ToList();
            return View(site);
        }

        #endregion

        #region Reports

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult ProgressPaymentList(string id)
        {
            BroadCast();
            site.M_USERS = db.T_USER.Where(t => t.UserTitleId == TitleEnum.UZMAN).ToList();
            site.term = id;
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult ProgressPaymentReport(int id)
        {
            BroadCast();
            string month1 = "";
            string currency1 = "";
            string month2 = "";
            string currency2 = "";
            decimal cumulated = 0;
            site.M_FILES = db.T_FILES.Where(t => t.WorkingUserId == id && t.Status == 7 && t.IsCancel == false).OrderByDescending(t => t.UpdateDate).ToList();
            site.ProcessPayment = site.M_FILES.Where(s => s.IsFree == false && s.IsCancel == false).Sum(t => t.Price2);
            site.ClosedFiles = site.M_FILES.Count();
            site.M_USER_STAT = db.T_USER_STATS.Where(t => t.UserId == id).FirstOrDefault();
            site.Rating = (site.M_USER_STAT.ClosedFileCount == 0 ? 0 : site.M_USER_STAT.TotalRating / site.M_USER_STAT.ClosedFileCount);

            var data = db.T_FILES.Where(t => t.WorkingUserId == id && t.Status == 7 && t.IsCancel == false).Select(k => new { k.UpdateDate.Value.Year, k.UpdateDate.Value.Month, k.Price2, k.IsFree }).GroupBy(x => new { x.Year, x.Month, x.IsFree }, (key, group) => new
            {
                yr = key.Year,
                mnth = key.Month,
                tCharge = group.Sum(k => k.Price2),
                IsFree = key.IsFree
            }).ToList();

            site.M_REPORT_PROGRESS = _Common.ToDataTable(data);
            site.M_REPORT_CUMULATED = _Common.ToDataTable(data);

            for (int i = 0; i < site.M_REPORT_PROGRESS.Rows.Count; i++)
            {
                month1 += "'" + _Common.GetMonth(site.M_REPORT_PROGRESS.Rows[i][1].ToString()) + "'" + ",";
                if ((bool)site.M_REPORT_PROGRESS.Rows[i][3] == false)
                {
                    currency1 += site.M_REPORT_PROGRESS.Rows[i][2].ToString().Replace(',', '.') + ",";
                    cumulated += Convert.ToDecimal(site.M_REPORT_CUMULATED.Rows[i][2]);
                }
                else
                {
                    currency1 += "0,";
                    cumulated += 0;
                }
                month2 += "'" + _Common.GetMonth(site.M_REPORT_CUMULATED.Rows[i][1].ToString()) + "'" + ",";
                currency2 += cumulated.ToString().Replace(',', '.') + ",";
            }

            site.month1 = month1.Length > 0 ? (month1.Remove(month1.Length - 1)) : "";
            site.currency1 = currency1.Length > 0 ? currency1.Remove(currency1.Length - 1) : "";

            site.month2 = month2.Length > 0 ? month2.Remove(month2.Length - 1) : "";
            site.currency2 = currency2.Length > 0 ? currency2.Remove(currency2.Length - 1) : "";

            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult CostPaymentList(string id)
        {
            BroadCast();
            site.M_CUSTOMERS = db.T_CUSTOMER.Where(t => t.CustomerSector != "1000").ToList();
            site.term = id;
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult CostPaymentReport(int id)
        {
            BroadCast();
            string month1 = "";
            string currency1 = "";
            string month2 = "";
            string currency2 = "";
            decimal cumulated = 0;
            site.M_FILES = db.T_FILES.Where(t => t.CreaterCustomerId == id && t.Status == 7 && t.IsCancel == false).OrderByDescending(t => t.UpdateDate).ToList();
            site.CostPayment = site.M_FILES.Where(s => s.IsFree == false && s.IsCancel == false).Sum(t => t.Price1);
            site.ClosedFiles = site.M_FILES.Count();
            site.M_CUSTOMER_STAT = db.T_CUSTOMER_STATS.Where(t => t.CustomerId == id).FirstOrDefault();
            site.Rating = (site.M_CUSTOMER_STAT.ClosedFileCount == 0 ? 0 : site.M_CUSTOMER_STAT.TotalRating / site.M_CUSTOMER_STAT.ClosedFileCount);

            var data = db.T_FILES.Where(t => t.CreaterCustomerId == id && t.Status == 7 && t.IsCancel == false).Select(k => new { k.UpdateDate.Value.Year, k.UpdateDate.Value.Month, k.Price1, k.IsFree }).GroupBy(x => new { x.Year, x.Month, x.IsFree }, (key, group) => new
            {
                yr = key.Year,
                mnth = key.Month,
                tCharge = group.Sum(k => k.Price1),
                IsFree = key.IsFree
            }).ToList();

            site.M_REPORT_PROGRESS = _Common.ToDataTable(data);
            site.M_REPORT_CUMULATED = _Common.ToDataTable(data);

            for (int i = 0; i < site.M_REPORT_PROGRESS.Rows.Count; i++)
            {
                month1 += "'" + _Common.GetMonth(site.M_REPORT_PROGRESS.Rows[i][1].ToString()) + "'" + ",";
                if ((bool)site.M_REPORT_PROGRESS.Rows[i][3] == false)
                {
                    currency1 += site.M_REPORT_PROGRESS.Rows[i][2].ToString().Replace(',', '.') + ",";
                    cumulated += Convert.ToDecimal(site.M_REPORT_CUMULATED.Rows[i][2]);
                }
                else
                {
                    currency1 += "0,";
                    cumulated += 0;
                }
                month2 += "'" + _Common.GetMonth(site.M_REPORT_CUMULATED.Rows[i][1].ToString()) + "'" + ",";
                currency2 += cumulated.ToString().Replace(',', '.') + ",";
            }

            site.month1 = month1.Length > 0 ? (month1.Remove(month1.Length - 1)) : "";
            site.currency1 = currency1.Length > 0 ? currency1.Remove(currency1.Length - 1) : "";

            site.month2 = month2.Length > 0 ? month2.Remove(month2.Length - 1) : "";
            site.currency2 = currency2.Length > 0 ? currency2.Remove(currency2.Length - 1) : "";

            return View(site);
        }

        #endregion

        #region Stakeholders

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult StakeholdersList(string id)
        {
            BroadCast();
            if (_Users.CurrentCustomerOwner == true)
            {
                if (_Users.CurrentManage == false)
                {
                    site.M_CUSTOMER_OWNERS = db.T_CUSTOMER_OWNER.Where(t => t.OwnerUserId == _Users.CurrentId).ToList();
                }
                else
                {
                    site.M_CUSTOMER_OWNERS = db.T_CUSTOMER_OWNER.ToList();
                }
            }
            else
            {
                return Redirect("/Operator/Dashboard");
            }
            
            site.term = id;
            return View(site);
        }

        #endregion

        #region Advice

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult Advice()
        {
            BroadCast();
            site.M_ADVICES = db.T_ADVICE.OrderByDescending(t => t.CreateDate).ToList();
            return View(site);
        }

        [ErrorLog]
        [Authorize(Roles = "3")]
        public ActionResult EditAdvice(int id)
        {
            BroadCast();
            T_ADVICE adv = db.T_ADVICE.Where(t => t.Id == id).FirstOrDefault();
            adv.IsRead = true;
            db.SaveChanges();
            site.M_ADVICE = adv;
            return View(site);
        }

        [ErrorLog]
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "3")]
        public ActionResult EditAdvice(int id, string OperatorExp)
        {
            T_ADVICE adv = db.T_ADVICE.Where(t => t.Id == id).FirstOrDefault();
            adv.OperatorExp = OperatorExp;
            adv.UpdateDate = DateTime.Now;

            db.SaveChanges();

            return Redirect("/Operator/Advice");
        }

        #endregion

        #region Search

        [ErrorLog]
        [Authorize(Roles = "3")]
        public JsonResult getFileSearch(string id)
        {
            return Json(db.T_FILES.Where(t => t.Id.ToString().StartsWith(id)).OrderBy(o => o.Id).Select(s => new { Id = s.Id + "( " + s.T_STATUS.StatuName + " )", Link = "/Operator/FilesDetails/" + s.Id }), JsonRequestBehavior.AllowGet);
        }

        #endregion

        #region BroadCast

        [ErrorLog]
        private void BroadCast()
        {
            Statu = new string[] { "25" };
            site.M_BROADCAST_FILES = db.T_FILES.Where(t => Statu.Contains(t.T_STATUS.StatuCode.ToString()) && t.FileRead == true).OrderByDescending(t => t.UpdateDate).ToList();
            site.Controller = "Operator";
        }

        #endregion
    }
}