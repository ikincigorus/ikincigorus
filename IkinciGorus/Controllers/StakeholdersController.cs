﻿using IkinciGorus.Models;
using System.Linq;
using System.Web.Mvc;

namespace IkinciGorus.Controllers
{
    public class StakeholdersController : Controller
    {
        // GET: Stakeholders

        _Model site = new _Model();
        ContextDb db = new ContextDb();

        [ErrorLog]
        [Authorize]
        public ActionResult Index(string id)
        {
            if (_Users.CurrentCustomerOwner == false)
                Response.Redirect("/");

            site.M_CUSTOMER_OWNERS = db.T_CUSTOMER_OWNER.Where(t => t.OwnerUserId == _Users.CurrentId).ToList();
            site.term = id;

            return View(site);
        }
    }
}