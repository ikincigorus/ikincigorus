﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IkinciGorus.Enums
{
    public class SectorEnum
    {
        public static int SAGLIK = 2;
        public static int HUKKUK = 3;
        public static int SIGORTA = 4;
    }

    public class TitleEnum
    {
        public static int UZMAN = 1;
        public static int MUSTERI = 2;
        public static int OPERATOR = 3;
    }
}