﻿using IkinciGorus.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Security.Principal;
using System.Web;
using System.Web.Configuration;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using System.Web.Security;

namespace IkinciGorus
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            if (WebConfigurationManager.AppSettings["JobStart"].ToString() == "1")
            {
                _JobScheduler.Start();
            }
        }

        protected void Application_AuthenticateRequest(Object sender, EventArgs e)
        {
            HttpCookie authCookie = Context.Request.Cookies[FormsAuthentication.FormsCookieName];
            if (authCookie == null || authCookie.Value == "")
                return;

            FormsAuthenticationTicket authTicket;
            try
            {
                authTicket = FormsAuthentication.Decrypt(authCookie.Value);
            }
            catch
            {
                return;
            }

            // retrieve roles from UserData
            string[] roles = authTicket.UserData.Split(';');

            if (Context.User != null)
                Context.User = new GenericPrincipal(Context.User.Identity, roles);
        }

        protected void Application_BeginRequest()
        {
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.Cache.SetExpires(DateTime.UtcNow.AddHours(-1));
            Response.Cache.SetNoStore();
        }

        protected void Application_PreSendRequestHeaders()
        {
            string gelenUrl = HttpContext.Current.Request.Url.ToString();
            string url = HttpContext.Current.Request.Url.ToString();
            //gelenUrl += "Dışarda :" + url + Environment.NewLine;
            if (url == "http://online.ikincigoz.net/login/signup")
            //if (url.IndexOf("login/signup") > 0)
            {
                //gelenUrl += "İçerde :" + url + Environment.NewLine;
                Response.Headers.Remove("X-Frame-Options");
                Response.AddHeader("X-Frame-Options", "AllowAll");
            }
            //System.IO.File.AppendAllText(Server.MapPath("/Log/Text.txt"), gelenUrl + Environment.NewLine);
        }
    }
}
