namespace IkinciGorus.Models
{
    using System;
    using System.Data.Entity;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Linq;

    public partial class ContextDb : DbContext
    {
        public ContextDb()
            : base("name=ContextDb")
        {
        }

        public virtual DbSet<T_ADVICE> T_ADVICE { get; set; }
        public virtual DbSet<T_BRANCH> T_BRANCH { get; set; }
        public virtual DbSet<T_CITY> T_CITY { get; set; }
        public virtual DbSet<T_CUSTOMER> T_CUSTOMER { get; set; }
        public virtual DbSet<T_CUSTOMER_OWNER> T_CUSTOMER_OWNER { get; set; }
        public virtual DbSet<T_CUSTOMER_STATS> T_CUSTOMER_STATS { get; set; }
        public virtual DbSet<T_DOCUMENTS> T_DOCUMENTS { get; set; }
        public virtual DbSet<T_FILES> T_FILES { get; set; }
        public virtual DbSet<T_HOSPITAL> T_HOSPITAL { get; set; }
        public virtual DbSet<T_MATERIAL> T_MATERIAL { get; set; }
        public virtual DbSet<T_NEW_USER_REQUEST> T_NEW_USER_REQUEST { get; set; }
        public virtual DbSet<T_PRICEPROCCALCTYPE> T_PRICEPROCCALCTYPE { get; set; }
        public virtual DbSet<T_PRICEPROTOCOL> T_PRICEPROTOCOL { get; set; }
        public virtual DbSet<T_SECTOR> T_SECTOR { get; set; }
        public virtual DbSet<T_STATUS> T_STATUS { get; set; }
        public virtual DbSet<T_TITLE> T_TITLE { get; set; }
        public virtual DbSet<T_TOWN> T_TOWN { get; set; }
        public virtual DbSet<T_USER> T_USER { get; set; }
        public virtual DbSet<T_USER_STATS> T_USER_STATS { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<T_BRANCH>()
                .Property(e => e.BranchName)
                .IsUnicode(false);

            modelBuilder.Entity<T_BRANCH>()
                .HasMany(e => e.T_FILES)
                .WithOptional(e => e.T_BRANCH)
                .HasForeignKey(e => e.BranchFileId);

            modelBuilder.Entity<T_BRANCH>()
                .HasMany(e => e.T_USER)
                .WithOptional(e => e.T_BRANCH)
                .HasForeignKey(e => e.BranchUserId);

            modelBuilder.Entity<T_CITY>()
                .HasMany(e => e.T_CUSTOMER)
                .WithOptional(e => e.T_CITY)
                .HasForeignKey(e => e.City);

            modelBuilder.Entity<T_CITY>()
                .HasMany(e => e.T_HOSPITAL)
                .WithOptional(e => e.T_CITY)
                .HasForeignKey(e => e.City);

            modelBuilder.Entity<T_CITY>()
                .HasMany(e => e.T_TOWN)
                .WithOptional(e => e.T_CITY)
                .HasForeignKey(e => e.CityId);

            modelBuilder.Entity<T_CUSTOMER>()
                .Property(e => e.CustomerGuid)
                .IsUnicode(false);

            modelBuilder.Entity<T_CUSTOMER>()
                .Property(e => e.TitleName)
                .IsUnicode(false);

            modelBuilder.Entity<T_CUSTOMER>()
                .Property(e => e.Adress)
                .IsUnicode(false);

            modelBuilder.Entity<T_CUSTOMER>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<T_CUSTOMER>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<T_CUSTOMER>()
                .HasMany(e => e.T_CUSTOMER_OWNER)
                .WithOptional(e => e.T_CUSTOMER)
                .HasForeignKey(e => e.OwnerCustomerId);

            modelBuilder.Entity<T_CUSTOMER>()
                .HasMany(e => e.T_CUSTOMER_STATS)
                .WithOptional(e => e.T_CUSTOMER)
                .HasForeignKey(e => e.CustomerId);

            modelBuilder.Entity<T_CUSTOMER>()
                .HasMany(e => e.T_FILES)
                .WithOptional(e => e.T_CUSTOMER)
                .HasForeignKey(e => e.CreaterCustomerId);

            modelBuilder.Entity<T_CUSTOMER>()
                .HasMany(e => e.T_USER)
                .WithOptional(e => e.T_CUSTOMER)
                .HasForeignKey(e => e.CustomerId);

            modelBuilder.Entity<T_FILES>()
                .Property(e => e.PatientName)
                .IsUnicode(false);

            modelBuilder.Entity<T_FILES>()
                .Property(e => e.InstitutionPatientId)
                .IsUnicode(false);

            modelBuilder.Entity<T_FILES>()
                .HasMany(e => e.T_DOCUMENTS)
                .WithOptional(e => e.T_FILES)
                .HasForeignKey(e => e.FileId);

            modelBuilder.Entity<T_HOSPITAL>()
                .Property(e => e.TitleName)
                .IsUnicode(false);

            modelBuilder.Entity<T_HOSPITAL>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<T_HOSPITAL>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<T_HOSPITAL>()
                .HasMany(e => e.T_FILES)
                .WithOptional(e => e.T_HOSPITAL)
                .HasForeignKey(e => e.HospitalId);

            modelBuilder.Entity<T_MATERIAL>()
                .Property(e => e.MaterialName)
                .IsUnicode(false);

            modelBuilder.Entity<T_NEW_USER_REQUEST>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<T_NEW_USER_REQUEST>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<T_NEW_USER_REQUEST>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<T_NEW_USER_REQUEST>()
                .Property(e => e.OperationComment)
                .IsUnicode(false);

            modelBuilder.Entity<T_PRICEPROCCALCTYPE>()
                .HasMany(e => e.T_PRICEPROTOCOL)
                .WithOptional(e => e.T_PRICEPROCCALCTYPE)
                .HasForeignKey(e => e.CalculationTypeId);

            modelBuilder.Entity<T_PRICEPROTOCOL>()
                .Property(e => e.ProtocolName)
                .IsUnicode(false);

            modelBuilder.Entity<T_PRICEPROTOCOL>()
                .HasMany(e => e.T_CUSTOMER)
                .WithOptional(e => e.T_PRICEPROTOCOL)
                .HasForeignKey(e => e.PriceProtocolHealthId);

            modelBuilder.Entity<T_PRICEPROTOCOL>()
                .HasMany(e => e.T_USER)
                .WithOptional(e => e.T_PRICEPROTOCOL)
                .HasForeignKey(e => e.PriceProtocolUserId);

            modelBuilder.Entity<T_SECTOR>()
                .HasMany(e => e.T_BRANCH)
                .WithOptional(e => e.T_SECTOR)
                .HasForeignKey(e => e.BranchSectorId);

            modelBuilder.Entity<T_SECTOR>()
                .HasMany(e => e.T_FILES)
                .WithOptional(e => e.T_SECTOR)
                .HasForeignKey(e => e.FilesSectorId);

            modelBuilder.Entity<T_SECTOR>()
                .HasMany(e => e.T_PRICEPROCCALCTYPE)
                .WithOptional(e => e.T_SECTOR)
                .HasForeignKey(e => e.CalcTypeSectorId);

            modelBuilder.Entity<T_SECTOR>()
                .HasMany(e => e.T_PRICEPROTOCOL)
                .WithOptional(e => e.T_SECTOR)
                .HasForeignKey(e => e.PriProtSectorId);

            modelBuilder.Entity<T_SECTOR>()
                .HasMany(e => e.T_USER)
                .WithOptional(e => e.T_SECTOR)
                .HasForeignKey(e => e.UserSectorId);

            modelBuilder.Entity<T_STATUS>()
                .HasMany(e => e.T_FILES)
                .WithOptional(e => e.T_STATUS)
                .HasForeignKey(e => e.Status);

            modelBuilder.Entity<T_TITLE>()
                .HasMany(e => e.T_USER)
                .WithOptional(e => e.T_TITLE)
                .HasForeignKey(e => e.UserTitleId);

            modelBuilder.Entity<T_USER>()
                .Property(e => e.UserGuid)
                .IsUnicode(false);

            modelBuilder.Entity<T_USER>()
                .Property(e => e.Name)
                .IsUnicode(false);

            modelBuilder.Entity<T_USER>()
                .Property(e => e.Phone)
                .IsUnicode(false);

            modelBuilder.Entity<T_USER>()
                .Property(e => e.Email)
                .IsUnicode(false);

            modelBuilder.Entity<T_USER>()
                .Property(e => e.Password)
                .IsUnicode(false);

            modelBuilder.Entity<T_USER>()
                .HasMany(e => e.T_ADVICE)
                .WithOptional(e => e.T_USER)
                .HasForeignKey(e => e.AdviceUserId);

            modelBuilder.Entity<T_USER>()
                .HasMany(e => e.T_CUSTOMER_OWNER)
                .WithOptional(e => e.T_USER)
                .HasForeignKey(e => e.OwnerUserId);

            modelBuilder.Entity<T_USER>()
                .HasMany(e => e.T_FILES)
                .WithOptional(e => e.T_USER)
                .HasForeignKey(e => e.CreaterUserId);

            modelBuilder.Entity<T_USER>()
                .HasMany(e => e.T_FILES1)
                .WithOptional(e => e.T_USER1)
                .HasForeignKey(e => e.WorkingUserId);

            modelBuilder.Entity<T_USER>()
                .HasMany(e => e.T_USER_STATS)
                .WithOptional(e => e.T_USER)
                .HasForeignKey(e => e.UserId);
        }
    }
}
