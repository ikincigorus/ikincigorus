﻿using System;
using System.Web.Mvc;

namespace IkinciGorus.Models
{
    public class ErrorLogAttribute : HandleErrorAttribute
    {
        public static readonly string ExceptionFormatString = "<b>Controller:</b>{0}<br/><b>Action:</b>{1}<br/><b>Date:</b>{2}<br/><b>Type:</b>{3}<br/><b>ExceptionMessage:</b>{4}";

        public override void OnException(ExceptionContext filterContext)
        {

            string exDetail = string.Format(ExceptionFormatString,
                                            filterContext.RouteData.Values["controller"].ToString(), 
                                            filterContext.RouteData.Values["action"].ToString(),
                                            DateTime.Now,
                                            filterContext.Exception.GetType().ToString(),
                                            GetInnerException(filterContext.Exception).Message
                                            );

            _Common.SystemSendMail("İkinci Göz sisteminde hata oldu --> " + DateTime.Now, exDetail);
        }

        private Exception GetInnerException(Exception exception)
        {
            if (exception.InnerException != null)
            {
                return GetInnerException(exception.InnerException);
            }
            else
                return exception;
        }
    }
}