namespace IkinciGorus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_ADVICE
    {
        public long Id { get; set; }

        public long? AdviceUserId { get; set; }

        [StringLength(4000)]
        public string AdviceExp { get; set; }

        public bool? IsRead { get; set; }

        [StringLength(4000)]
        public string OperatorExp { get; set; }

        public DateTime? UpdateDate { get; set; }

        public DateTime? CreateDate { get; set; }

        public virtual T_USER T_USER { get; set; }
    }
}
