namespace IkinciGorus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_CUSTOMER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_CUSTOMER()
        {
            T_CUSTOMER_OWNER = new HashSet<T_CUSTOMER_OWNER>();
            T_CUSTOMER_STATS = new HashSet<T_CUSTOMER_STATS>();
            T_FILES = new HashSet<T_FILES>();
            T_USER = new HashSet<T_USER>();
        }

        public long Id { get; set; }

        [StringLength(36)]
        public string CustomerGuid { get; set; }

        [StringLength(5)]
        public string CustomerSector { get; set; }

        [StringLength(200)]
        public string TitleName { get; set; }

        public long? Vkn { get; set; }

        [StringLength(300)]
        public string Adress { get; set; }

        public int? City { get; set; }

        [StringLength(15)]
        public string Phone { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        public int? Status { get; set; }

        public long? PriceProtocolHealthId { get; set; }

        public long? PriceProtocolLawId { get; set; }

        public long? PriceProtocolInsuranceId { get; set; }

        public int? DiscountRate { get; set; }

        public int ValorDay { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public virtual T_CITY T_CITY { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_CUSTOMER_OWNER> T_CUSTOMER_OWNER { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_CUSTOMER_STATS> T_CUSTOMER_STATS { get; set; }

        public virtual T_PRICEPROTOCOL T_PRICEPROTOCOL { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_FILES> T_FILES { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_USER> T_USER { get; set; }
    }
}
