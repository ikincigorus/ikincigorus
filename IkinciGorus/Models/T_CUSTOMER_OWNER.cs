namespace IkinciGorus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_CUSTOMER_OWNER
    {
        public long Id { get; set; }

        public long? OwnerCustomerId { get; set; }

        public long? OwnerUserId { get; set; }

        public double? OwnerRate { get; set; }

        public virtual T_CUSTOMER T_CUSTOMER { get; set; }

        public virtual T_USER T_USER { get; set; }
    }
}
