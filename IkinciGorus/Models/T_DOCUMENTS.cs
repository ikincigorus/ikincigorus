namespace IkinciGorus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_DOCUMENTS
    {
        public long Id { get; set; }

        public long? FileId { get; set; }

        [StringLength(100)]
        public string ContentName { get; set; }

        [StringLength(100)]
        public string ContentType { get; set; }

        public byte[] Contents { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public virtual T_FILES T_FILES { get; set; }
    }
}
