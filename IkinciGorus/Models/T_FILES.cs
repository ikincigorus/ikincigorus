namespace IkinciGorus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_FILES
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_FILES()
        {
            T_DOCUMENTS = new HashSet<T_DOCUMENTS>();
        }

        public long Id { get; set; }

        public int? FilesSectorId { get; set; }

        [StringLength(30)]
        public string PatientName { get; set; }

        [StringLength(20)]
        public string InstitutionPatientId { get; set; }

        public long? HospitalId { get; set; }

        public long? BranchFileId { get; set; }

        public long? CreaterUserId { get; set; }

        public long? CreaterCustomerId { get; set; }

        public long? WorkingUserId { get; set; }

        public string FileDescription { get; set; }

        public string DoctorDescription { get; set; }

        public bool? FileWaiting { get; set; }

        public decimal? DemandAmount { get; set; }

        public decimal? EvaluatedAmount { get; set; }

        public bool? IsFree { get; set; }

        public decimal? Price1 { get; set; }

        public decimal? Price2 { get; set; }

        public bool? FileCargo { get; set; }

        [StringLength(250)]
        public string FileUpload { get; set; }

        public long? Status { get; set; }

        public bool? FileRead { get; set; }

        public int? Rating { get; set; }

        [StringLength(500)]
        public string RatingDescription { get; set; }

        public bool? RollBackFile { get; set; }

        public bool? IsCancel { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        public virtual T_BRANCH T_BRANCH { get; set; }

        public virtual T_CUSTOMER T_CUSTOMER { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_DOCUMENTS> T_DOCUMENTS { get; set; }

        public virtual T_HOSPITAL T_HOSPITAL { get; set; }

        public virtual T_SECTOR T_SECTOR { get; set; }

        public virtual T_STATUS T_STATUS { get; set; }

        public virtual T_USER T_USER { get; set; }

        public virtual T_USER T_USER1 { get; set; }
    }
}
