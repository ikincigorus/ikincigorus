namespace IkinciGorus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_FILES_R
    {
        [Key]
        public long Id { get; set; }

        public int SectorId { get; set; }

        [StringLength(30)]
        public string PatientName { get; set; }

        [StringLength(20)]
        public string InstitutionPatientId { get; set; }

        public long? HospitalId { get; set; }

        public long? BranchFileId { get; set; }

        public long? CreaterUserId { get; set; }

        public long? CreaterCustomerId { get; set; }

        public long? WorkingUserId { get; set; }

        public string FileDescription { get; set; }

        public string DoctorDescription { get; set; }

        public bool FileWaiting { get; set; }

        public string DemandAmount { get; set; }
        
        public string EvaluatedAmount { get; set; }

        public bool? IsFree { get; set; }

        public string Price1 { get; set; }

        public string Price2 { get; set; }

        public bool? FileCargo { get; set; }

        public string FileUpload { get; set; }

        public long? Status { get; set; }

        public bool FileRead { get; set; }

        public int Rating { get; set; }

        public string RatingDescription { get; set; }

        public bool RollBackFile { get; set; }

        public bool IsCancel { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }
    }
}
