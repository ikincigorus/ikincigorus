namespace IkinciGorus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_MATERIAL
    {
        public long Id { get; set; }

        [StringLength(200)]
        public string MaterialName { get; set; }

        public decimal? UnitPrice { get; set; }

        public int? UseNumber { get; set; }

        public DateTime? ValidityDate { get; set; }

        [StringLength(50)]
        public string UbbUts { get; set; }

        [StringLength(50)]
        public string Sut { get; set; }

        public int? Status { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }
    }
}
