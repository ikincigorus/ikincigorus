namespace IkinciGorus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_NEW_USER_REQUEST
    {
        public long Id { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        [StringLength(15)]
        public string Phone { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        public int? CommunicationChoice { get; set; }

        [StringLength(5)]
        public string NewUserSector { get; set; }

        public int? Statu { get; set; }

        [StringLength(300)]
        public string OperationComment { get; set; }

        public bool? UserRead { get; set; }

        public DateTime? CreateDate { get; set; }
    }
}
