namespace IkinciGorus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_PRICEPROCCALCTYPE
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_PRICEPROCCALCTYPE()
        {
            T_PRICEPROTOCOL = new HashSet<T_PRICEPROTOCOL>();
        }

        public int Id { get; set; }

        public int? CalcTypeSectorId { get; set; }

        [StringLength(50)]
        public string CalcTypeName { get; set; }

        public virtual T_SECTOR T_SECTOR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_PRICEPROTOCOL> T_PRICEPROTOCOL { get; set; }
    }
}
