namespace IkinciGorus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_PRICEPROTOCOL
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_PRICEPROTOCOL()
        {
            T_CUSTOMER = new HashSet<T_CUSTOMER>();
            T_USER = new HashSet<T_USER>();
        }

        public long Id { get; set; }

        public int? PriProtSectorId { get; set; }

        [StringLength(200)]
        public string ProtocolName { get; set; }

        public int? CalculationTypeId { get; set; }

        public decimal? UnitPrice { get; set; }

        public DateTime? ValidityDate { get; set; }

        public int? Status { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_CUSTOMER> T_CUSTOMER { get; set; }

        public virtual T_PRICEPROCCALCTYPE T_PRICEPROCCALCTYPE { get; set; }

        public virtual T_SECTOR T_SECTOR { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_USER> T_USER { get; set; }
    }
}
