namespace IkinciGorus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_SECTOR
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_SECTOR()
        {
            T_BRANCH = new HashSet<T_BRANCH>();
            T_FILES = new HashSet<T_FILES>();
            T_PRICEPROCCALCTYPE = new HashSet<T_PRICEPROCCALCTYPE>();
            T_PRICEPROTOCOL = new HashSet<T_PRICEPROTOCOL>();
            T_USER = new HashSet<T_USER>();
        }

        public int Id { get; set; }

        [StringLength(50)]
        public string Sector { get; set; }

        [StringLength(50)]
        public string InputName { get; set; }

        [StringLength(50)]
        public string TagName { get; set; }

        public bool? Hide { get; set; }

        public bool? Active { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_BRANCH> T_BRANCH { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_FILES> T_FILES { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_PRICEPROCCALCTYPE> T_PRICEPROCCALCTYPE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_PRICEPROTOCOL> T_PRICEPROTOCOL { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_USER> T_USER { get; set; }
    }
}
