namespace IkinciGorus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_USER
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public T_USER()
        {
            T_ADVICE = new HashSet<T_ADVICE>();
            T_CUSTOMER_OWNER = new HashSet<T_CUSTOMER_OWNER>();
            T_FILES = new HashSet<T_FILES>();
            T_FILES1 = new HashSet<T_FILES>();
            T_USER_STATS = new HashSet<T_USER_STATS>();
        }

        public long Id { get; set; }

        [StringLength(36)]
        public string UserGuid { get; set; }

        public int? UserSectorId { get; set; }

        public int? UserTitleId { get; set; }

        [StringLength(100)]
        public string Name { get; set; }

        public int? Gender { get; set; }

        public DateTime? BirthDate { get; set; }

        [StringLength(15)]
        public string Phone { get; set; }

        [StringLength(100)]
        public string Email { get; set; }

        public int? Status { get; set; }

        public long? CustomerId { get; set; }

        public long? PriceProtocolUserId { get; set; }

        public long? BranchUserId { get; set; }

        [StringLength(500)]
        public string Password { get; set; }

        public int? PasswordCount { get; set; }

        public DateTime? PasswordSetDate { get; set; }

        public bool? Manage { get; set; }

        public bool? CustomerOwner { get; set; }

        public bool? Blocked { get; set; }

        public DateTime? CreateDate { get; set; }

        public DateTime? UpdateDate { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_ADVICE> T_ADVICE { get; set; }

        public virtual T_BRANCH T_BRANCH { get; set; }

        public virtual T_CUSTOMER T_CUSTOMER { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_CUSTOMER_OWNER> T_CUSTOMER_OWNER { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_FILES> T_FILES { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_FILES> T_FILES1 { get; set; }

        public virtual T_PRICEPROTOCOL T_PRICEPROTOCOL { get; set; }

        public virtual T_SECTOR T_SECTOR { get; set; }

        public virtual T_TITLE T_TITLE { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<T_USER_STATS> T_USER_STATS { get; set; }
    }
}
