namespace IkinciGorus.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    public partial class T_USER_STATS
    {
        public long Id { get; set; }

        public long? UserId { get; set; }

        public long? ClosedFileCount { get; set; }

        public long? TotalRating { get; set; }

        public DateTime? LastUpdateDate { get; set; }

        public virtual T_USER T_USER { get; set; }
    }
}
