﻿using IkinciGorus.Enums;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web.Configuration;

namespace IkinciGorus.Models
{
    public static class _Common
    {

        public static string GetHashString(string inputString)
        {
            StringBuilder sb = new StringBuilder();
            foreach (byte b in GetHash(inputString))
                sb.Append(b.ToString("X2"));

            return sb.ToString();
        }

        public static string MaskName(string inputString)
        {
            string[] arr = inputString.Split(' ');
            string retVal = "";
            for (int i = 0; i < arr.Length; i++)
            {
                for (int j = 0; j < arr[i].Length; j++)
                {
                    if (j == 0)
                        retVal += arr[i].ToCharArray()[0];
                    else
                        retVal += '*';
                }
                if (i < arr.Length - 1)
                    retVal += ' ';
            }


            return retVal;
        }

        private static byte[] GetHash(string inputString)
        {
            using (HashAlgorithm algorithm = SHA256.Create())
                return algorithm.ComputeHash(Encoding.UTF8.GetBytes(inputString));
        }

        //public T_USER LoginUser() => site.M_USER = (T_USER)HttpContext.Current.Session["user"];

        public static string GetUrl(int? dashboard, string returnUrl)
        {
            if (returnUrl == null)
            {
                switch (dashboard)
                {
                    case 1:
                        return "/Expert/Dashboard";
                    case 2:
                        return "/Customer/Dashboard";
                    case 3:
                        return "/Operator/Dashboard";
                    case 4:
                        return "/Operator/Dashboard";
                    default:
                        return "/Login";
                }
            }
            else
            {
                return returnUrl;
            }
        }

        public static void SendMail(string to, string subject, string body)
        {
            MailSender(to, subject, body, true);
        }

        public static void SystemSendMail(string subject, string body)
        {
            MailSender("ikincigorus2@gmail.com", subject, body, true);
        }

        public static void MailSender(string to, string subject, string body, bool bcc)
        {
            SmtpClient client = new SmtpClient();
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.EnableSsl = true;
            client.Host = "smtp.gmail.com";
            client.Port = 587;

            NetworkCredential credentials = new NetworkCredential("ikincigorus2@gmail.com", "sxxd1984");
            client.UseDefaultCredentials = false;
            client.Credentials = credentials;

            MailMessage msg = new MailMessage();
            msg.From = new MailAddress("info@ikincigorus.com", "Yönetim");
            msg.To.Add(new MailAddress(to));
            if (bcc == true) { 
                msg.Bcc.Add("oguzhan1981@gmail.com");
                msg.Bcc.Add("imaden@gmail.com");
            }
            msg.Subject = subject;
            msg.BodyEncoding = UTF8Encoding.UTF8;
            msg.IsBodyHtml = true;
            msg.Body = string.Format(body);
            try
            {
                client.Send(msg);
            }
            catch (Exception ex)
            {
                string exep = ex.Message;
            }
        }

        public static string CreatePassword(int length)
        {
            const string valid = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
            StringBuilder res = new StringBuilder();
            Random rnd = new Random();
            while (0 < length--)
            {
                res.Append(valid[rnd.Next(valid.Length)]);
            }
            return res.ToString();
        }

        public static bool CheckForSQLInjection(string userInput)
        {
            bool isSQLInjection = false;
            string[] sqlCheckList = { "--", ";--", ";", "/*", "*/", "@@", "@", "char", "nchar", "varchar", "nvarchar", "alter", "begin", "cast", "create", "cursor", "declare", "delete", "drop", "end", "exec", "execute", "fetch", "insert", "kill", "select", "sys", "sysobjects", "syscolumns", "table", "update" };
            string CheckString = userInput.Replace("'", "''");
            for (int i = 0; i <= sqlCheckList.Length - 1; i++)
            {
                if ((CheckString.IndexOf(sqlCheckList[i], StringComparison.OrdinalIgnoreCase) >= 0))
                    isSQLInjection = true;
            }
            return isSQLInjection;
        }

        public static string EnCryptIt(string toEnrypt)
        {
            if (!string.IsNullOrEmpty(toEnrypt))
            {
                try
                {
                    byte[] data = System.Text.ASCIIEncoding.ASCII.GetBytes(TurkishEnCode(toEnrypt));
                    byte[] rgbKey = System.Text.ASCIIEncoding.ASCII.GetBytes("01010101"); //buradaki string parametreler sizin crypto keyini’niz.
                    byte[] rgbIV = System.Text.ASCIIEncoding.ASCII.GetBytes("02020202"); //buradaki string parametreler sizin crypto value’nuz.
                    MemoryStream memoryStream = new MemoryStream(1024);
                    DESCryptoServiceProvider desCryptoServiceProvider = new DESCryptoServiceProvider();
                    CryptoStream cryptoStream = new CryptoStream(memoryStream, desCryptoServiceProvider.CreateEncryptor(rgbKey, rgbIV), CryptoStreamMode.Write);
                    cryptoStream.Write(data, 0, data.Length);
                    cryptoStream.FlushFinalBlock();
                    byte[] result = new byte[(int)memoryStream.Position];
                    memoryStream.Position = 0;
                    memoryStream.Read(result, 0, result.Length);
                    cryptoStream.Close();
                    return System.Convert.ToBase64String(result);
                }
                catch (CryptographicException ex)
                {
                    throw ex;
                }
            }
            return "";
        }

        public static string DeCryptIt(string toDecrypt)
        {
            string decrypted = string.Empty;
            if (!string.IsNullOrEmpty(toDecrypt))
            {
                try
                {
                    byte[] data = System.Convert.FromBase64String(toDecrypt);
                    byte[] rgbKey = System.Text.ASCIIEncoding.ASCII.GetBytes("01010101");
                    byte[] rgbIV = System.Text.ASCIIEncoding.ASCII.GetBytes("02020202");
                    MemoryStream memoryStream = new MemoryStream(data.Length);
                    DESCryptoServiceProvider desCryptoServiceProvider = new DESCryptoServiceProvider();
                    CryptoStream cryptoStream = new CryptoStream(memoryStream, desCryptoServiceProvider.CreateDecryptor(rgbKey, rgbIV), CryptoStreamMode.Read);
                    memoryStream.Write(data, 0, data.Length);
                    memoryStream.Position = 0;
                    decrypted = new StreamReader(cryptoStream).ReadToEnd();
                    cryptoStream.Close();
                }
                catch (CryptographicException ex)
                {
                    throw ex;
                }
            }
            return TurkishDeCode(decrypted);
        }

        public static string TurkishEnCode(string Text) // Türkçe karakteri İngilizce karaktere çevir
        {
            return Text.Replace("ı", "z001").Replace("İ", "z002").Replace("â", "z003").Replace("ç", "z004").Replace("Ç", "z005").
            Replace("ğ", "z006").Replace("Ğ", "z007").Replace("ö", "z008").Replace("Ö", "z009").
            Replace("ş", "z010").Replace("Ş", "z011").Replace("ü", "z012").Replace("Ü", "z013");
        }

        public static string TurkishDeCode(string Text) // Türkçe karakteri İngilizce karaktere çevir
        {
            return Text.Replace("z001", "ı").Replace("z002", "İ").Replace("z003", "â").Replace("z004", "ç").Replace("z005", "Ç").
            Replace("z006", "ğ").Replace("z007", "Ğ").Replace("z008", "ö").Replace("z009", "Ö").
            Replace("z010", "ş").Replace("z011", "Ş").Replace("z012", "ü").Replace("z013", "Ü");
        }

        public static decimal? PriceProtocolCalc(int? CalculationType, decimal? UnitPrice, decimal? Amount)
        {
            switch (CalculationType)
            {
                case 1:
                    return UnitPrice * Convert.ToDecimal(WebConfigurationManager.AppSettings["CalcTypeHUV"]);
                case 2:
                    return UnitPrice * Convert.ToDecimal(WebConfigurationManager.AppSettings["CalcTypeSerbest"]);
                case 3: // sayfa başı UnitPrice kurus
                    return (UnitPrice * Amount) / 100;
                default:
                    return 0;
            }
        }

        public static DataTable CommunicationChoiceList()
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[3] { new DataColumn("A"), new DataColumn("B"), new DataColumn("C") });
            dt.Rows.Add(1, "communicationchoice", "E-posta");
            dt.Rows.Add(2, "communicationchoice", "Telefon");
            return dt;
        }

        public static string GetCommunicationChoice(int? communicationchoice)
        {
            switch (communicationchoice)
            {
                case 1:
                    return "E-posta";
                case 2:
                    return "Telefon";
                default:
                    return "";
            }
        }

        public static DataTable StatuList()
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[3] { new DataColumn("A"), new DataColumn("B"), new DataColumn("C") });
            dt.Rows.Add(1, "statu", "Bekliyor");
            dt.Rows.Add(2, "statu", "İptal Edildi");
            dt.Rows.Add(3, "statu", "Tamamlandı");
            return dt;
        }

        public static string GetStatu(int statu)
        {
            switch (statu)
            {
                case 1:
                    return "Bekliyor";
                case 2:
                    return "İptal Edildi";
                case 3:
                    return "Tamamlandı";
                default:
                    return "";
            }
        }

        public static DataTable SectorLabel(int? id)
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[2] { new DataColumn("A"), new DataColumn("B") });

            if (id == SectorEnum.SAGLIK)
            {
                dt.Rows.Add(1, "Hasta Bilgileri");
                dt.Rows.Add(2, "Soru");
                dt.Rows.Add(3, "Belge / Kargo");
                dt.Rows.Add(4, "Dosya Önizleme");
                dt.Rows.Add(5, "Hasta Adı");
                dt.Rows.Add(6, "Kurum Hasta Numarası");
                dt.Rows.Add(7, "Hastane");
                dt.Rows.Add(8, "Branş");
                dt.Rows.Add(9, "Talep Tutarı");
                dt.Rows.Add(10, "Dosya Açıklaması");
                dt.Rows.Add(11, "Dosyalar");
                dt.Rows.Add(12, "Kargo var");
                dt.Rows.Add(13, "Doktor ve Operasyon Görüşü");
                dt.Rows.Add(14, "Dosya Transfer Adresi");
            }
            else if (id == SectorEnum.HUKKUK)
            {
                dt.Rows.Add(1, "Talep Bilgileri");
                dt.Rows.Add(2, "Soru");
                dt.Rows.Add(3, "Belge / Kargo");
                dt.Rows.Add(4, "Dosya Önizleme");
                dt.Rows.Add(5, "Talep Adı");
                dt.Rows.Add(6, "Kurum Talep Numarası");
                dt.Rows.Add(7, "Hastane");
                dt.Rows.Add(8, "Branş");
                dt.Rows.Add(9, "Dosya Sayfa Adedi");
                dt.Rows.Add(10, "Dosya Açıklaması");
                dt.Rows.Add(11, "Dosyalar");
                dt.Rows.Add(12, "Kargo var");
                dt.Rows.Add(13, "Avukat ve Operasyon Görüşü");
                dt.Rows.Add(14, "Dosya Transfer Adresi");
            }
            else if (id == SectorEnum.SIGORTA)
            {
                dt.Rows.Add(1, "Hasar Dosya Bilgileri");
                dt.Rows.Add(2, "Soru");
                dt.Rows.Add(3, "Belge / Kargo");
                dt.Rows.Add(4, "Dosya Önizleme");
                dt.Rows.Add(5, "Hasar Dosya Sahibi");
                dt.Rows.Add(6, "Kurum Dosya Numarası");
                dt.Rows.Add(7, "Hastane");
                dt.Rows.Add(8, "Branş");
                dt.Rows.Add(9, "Hasar Talep Tutarı");
                dt.Rows.Add(10, "Dosya Açıklaması");
                dt.Rows.Add(11, "Dosyalar");
                dt.Rows.Add(12, "Kargo var");
                dt.Rows.Add(13, "Eksper ve Operasyon Görüşü");
                dt.Rows.Add(14, "Dosya Transfer Adresi");
            }

            return dt;
        }

        public static DataTable ToDataTable<T>(this IList<T> data)
        {
            PropertyDescriptorCollection properties = TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

        public static string GetMonth(string month)
        {
            switch (month)
            {
                case "1":
                    return "Oca";
                case "2":
                    return "Şub";
                case "3":
                    return "Mar";
                case "4":
                    return "Nis";
                case "5":
                    return "May";
                case "6":
                    return "Haz";
                case "7":
                    return "Tem";
                case "8":
                    return "Ağu";
                case "9":
                    return "Eyl";
                case "10":
                    return "Eki";
                case "11":
                    return "Kas";
                case "12":
                    return "Ara";
                default:
                    return "";
            }
        }

        public static string OnlyLoginString(string userInput)
        {
            return Regex.Replace(userInput, "[^A-Za-z0-9@.]", "");
        }

        public static string OnlyCaptchaNumber(string userInput)
        {
            return Regex.Replace(userInput, "[^0-9]", "");
        }

        public static DataTable SectorExpertComment(int? id)
        {
            DataTable dt = new DataTable();
            dt.Columns.AddRange(new DataColumn[2] { new DataColumn("A"), new DataColumn("B") });

            if (id == SectorEnum.SAGLIK)
            {
                string str = "Yapılan ameliyat/İşlem adı ?\n\n\n\n";
                str += "Kesi kuralına göre faturalandırılmış mı ?\n\n\n\n";
                str += "Ameliyatlar bir birlerini kapsıyor mu ?\n\n\n\n";
                str += "Kullanılan özellikli malzeme uygun mu ?\n\n\n\n";
                str += "Özellikle malzeme olmadan da yapılabilir mi ?\n\n\n\n";
                str += "Özellikli malzemenin muadili var mı ?\n\n\n\n";
                str += "Özellikli malzeme bölünebilen bir malzeme mi ? Kaç defa kullanılabilinir ?\n\n\n\n";
                str += "Genel olarak değerlendirme\n\n";
                dt.Rows.Add(1, str);
            }
            else if (id == SectorEnum.HUKKUK)
            {
                string str = "Yasal açıdan değerlendirme?\n\n\n\n";
                str += "Genel olarak değerlendirme\n\n";
                dt.Rows.Add(1, str);
            }
            else if (id == SectorEnum.SIGORTA)
            {
                string str = "Hesaplanan hasar uygun mu?\n\n\n\n";
                str += "Muadil parça kullanımı mümkün mü?\n\n\n\n";
                str += "Genel olarak değerlendirme\n\n";
                dt.Rows.Add(1, str);
            }

            return dt;
        }


    }
}