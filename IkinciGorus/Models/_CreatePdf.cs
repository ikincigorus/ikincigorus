﻿using iTextSharp.text;
using iTextSharp.text.html;
using iTextSharp.text.pdf;
using System;
using System.Data;
using System.IO;
using System.Linq;
using System.Web;

namespace IkinciGorus.Models
{
    public static class _CreatePdf
    {
        public static void GenerateInvoice(T_FILES Data, DataTable SectorLabel)
        {
            Document pdfDoc = new Document(PageSize.A4, 10, 10, 10, 10);
            MemoryStream PDFData = new MemoryStream();
            PdfWriter writer = PdfWriter.GetInstance(pdfDoc, PDFData);

            BaseFont baseFont = BaseFont.CreateFont("C:\\Windows\\Fonts\\Arial.ttf", "windows-1254", true);

            Font titleFont = new Font(baseFont , 12, Font.BOLD);
            Font titleFontBlue = new Font(baseFont, 14, Font.NORMAL, BaseColor.BLUE);
            Font tableFont = new Font(baseFont, 10, Font.NORMAL);
            Font boldTableFont = new Font(baseFont, 10, Font.BOLD);
            Font bodyFont = new Font(baseFont, 8, Font.NORMAL);
            Font EmailFont = new Font(baseFont, 8, Font.NORMAL, BaseColor.BLUE);
            //BaseColor TabelHeaderBackGroundColor = WebColors.GetRGBColor("#EEEEEE");

            Rectangle pageSize = writer.PageSize;
            // Open the Document for writing
            pdfDoc.Open();
            //Add elements to the document here

            // Create the header table 
            PdfPTable headertable = new PdfPTable(3);
            headertable.HorizontalAlignment = 0;
            headertable.WidthPercentage = 100;
            headertable.SetWidths(new float[] { 100f, 310f, 110f });  // then set the column's __relative__ widths
            headertable.DefaultCell.Border = Rectangle.NO_BORDER;
            //headertable.DefaultCell.Border = Rectangle.BOX; //for testing           

            Image logo = Image.GetInstance(HttpContext.Current.Server.MapPath("~/content/images/pdf-logo.png"));
            logo.ScaleToFit(160, 50);
            {
                PdfPCell pdfCelllogo = new PdfPCell(logo);
                pdfCelllogo.Border = Rectangle.NO_BORDER;
                pdfCelllogo.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                pdfCelllogo.BorderWidthBottom = 1f;
                headertable.AddCell(pdfCelllogo);
            }

            {
                PdfPCell middlecell = new PdfPCell();
                middlecell.Border = Rectangle.NO_BORDER;
                middlecell.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                middlecell.BorderWidthBottom = 1f;
                headertable.AddCell(middlecell);
            }

            {
                PdfPTable nested = new PdfPTable(1);
                nested.DefaultCell.Border = Rectangle.NO_BORDER;
                PdfPCell nextPostCell1 = new PdfPCell(new Phrase("İkinci Göz", titleFont));
                nextPostCell1.Border = Rectangle.NO_BORDER;
                nested.AddCell(nextPostCell1);
                PdfPCell nextPostCell2 = new PdfPCell(new Phrase("Levent / İstanbul", bodyFont));
                nextPostCell2.Border = Rectangle.NO_BORDER;
                nested.AddCell(nextPostCell2);
                PdfPCell nextPostCell3 = new PdfPCell(new Phrase("(212) 000 0000", bodyFont));
                nextPostCell3.Border = Rectangle.NO_BORDER;
                nested.AddCell(nextPostCell3);
                PdfPCell nextPostCell4 = new PdfPCell(new Phrase("inceleme@ikincigoz.net", EmailFont));
                nextPostCell4.Border = Rectangle.NO_BORDER;
                nested.AddCell(nextPostCell4);
                nested.AddCell("");
                PdfPCell nesthousing = new PdfPCell(nested);
                nesthousing.Border = Rectangle.NO_BORDER;
                nesthousing.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                nesthousing.BorderWidthBottom = 1f;
                nesthousing.Rowspan = 5;
                nesthousing.PaddingBottom = 10f;
                headertable.AddCell(nesthousing);
            }


            PdfPTable Invoicetable = new PdfPTable(3);
            Invoicetable.HorizontalAlignment = 0;
            Invoicetable.WidthPercentage = 100;
            Invoicetable.SetWidths(new float[] { 100f, 310f, 110f });  // then set the column's __relative__ widths
            Invoicetable.DefaultCell.Border = Rectangle.NO_BORDER;

            {
                PdfPTable nested = new PdfPTable(1);
                nested.DefaultCell.Border = Rectangle.NO_BORDER;
                PdfPCell nextPostCell1 = new PdfPCell(new Phrase("Dosya Bilgileri:", bodyFont));
                nextPostCell1.Border = Rectangle.NO_BORDER;
                nested.AddCell(nextPostCell1);
                PdfPCell nextPostCell2 = new PdfPCell(new Phrase(Data.PatientName, titleFont));
                nextPostCell2.Border = Rectangle.NO_BORDER;
                nested.AddCell(nextPostCell2);
                PdfPCell nextPostCell3 = new PdfPCell(new Phrase(Data.T_SECTOR.Sector, bodyFont));
                nextPostCell3.Border = Rectangle.NO_BORDER;
                nested.AddCell(nextPostCell3);
                PdfPCell nextPostCell4 = new PdfPCell(new Phrase(Data.InstitutionPatientId, bodyFont));
                nextPostCell4.Border = Rectangle.NO_BORDER;
                nested.AddCell(nextPostCell4);
                nested.AddCell("");
                PdfPCell nesthousing = new PdfPCell(nested);
                nesthousing.Border = Rectangle.NO_BORDER;
                //nesthousing.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                //nesthousing.BorderWidthBottom = 1f;
                nesthousing.Rowspan = 5;
                nesthousing.PaddingBottom = 10f;
                Invoicetable.AddCell(nesthousing);
            }

            {
                PdfPCell middlecell = new PdfPCell();
                middlecell.Border = Rectangle.NO_BORDER;
                //middlecell.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                //middlecell.BorderWidthBottom = 1f;
                Invoicetable.AddCell(middlecell);
            }


            {
                PdfPTable nested = new PdfPTable(1);
                nested.DefaultCell.Border = Rectangle.NO_BORDER;
                PdfPCell nextPostCell1 = new PdfPCell(new Phrase(Data.Id.ToString(), titleFontBlue));
                nextPostCell1.Border = Rectangle.NO_BORDER;
                nested.AddCell(nextPostCell1);
                PdfPCell nextPostCell2 = new PdfPCell(new Phrase("Oluşturulma Tarihi: " + Convert.ToDateTime(Data.CreateDate.ToString()).ToString("dd.MM.yyyy"), bodyFont));
                nextPostCell2.Border = Rectangle.NO_BORDER;
                nested.AddCell(nextPostCell2);
                PdfPCell nextPostCell3 = new PdfPCell(new Phrase("Son İşlem Tarihi : " + Convert.ToDateTime(Data.UpdateDate.ToString()).ToString("dd.MM.yyyy"), bodyFont));
                nextPostCell3.Border = Rectangle.NO_BORDER;
                nested.AddCell(nextPostCell3);
                nested.AddCell("");
                PdfPCell nesthousing = new PdfPCell(nested);
                nesthousing.Border = Rectangle.NO_BORDER;
                //nesthousing.BorderColorBottom = new BaseColor(System.Drawing.Color.Black);
                //nesthousing.BorderWidthBottom = 1f;
                nesthousing.Rowspan = 5;
                nesthousing.PaddingBottom = 10f;
                Invoicetable.AddCell(nesthousing);
            }


            pdfDoc.Add(headertable);
            Invoicetable.PaddingTop = 10f;

            pdfDoc.Add(Invoicetable);
           
            //Create body table
            PdfPTable itemTable = new PdfPTable(2);

            itemTable.HorizontalAlignment = 0;
            itemTable.WidthPercentage = 100;
            itemTable.SetWidths(new float[] { 50, 50 });  // then set the column's __relative__ widths
            itemTable.SpacingAfter = 40;
            itemTable.DefaultCell.Border = Rectangle.NO_BORDER;
            PdfPCell cellustyazi = new PdfPCell(new Phrase("Sayın " + Data.T_USER.Name + " (" + Data.T_CUSTOMER.TitleName + ") " + ",\n\nŞirketimize " + Data.Id + " kayıt numarası ile gönderdiğiniz " + Data.PatientName + " ait " + (Data.FilesSectorId == 2 ? Data.T_HOSPITAL.TitleName + " " : "") + Data.T_BRANCH.BranchName + " dosyası uzmanımız tarafından ikinci bir gözle detaylı incelenmiştir.\n\nUzmanımızın görüşünü arz ederiz.", tableFont));
            cellustyazi.Colspan = 4;
            cellustyazi.HorizontalAlignment = 0;
            cellustyazi.PaddingBottom = 20f;
            cellustyazi.PaddingTop = 20f;
            cellustyazi.Border = Rectangle.NO_BORDER;
            itemTable.AddCell(cellustyazi);


            PdfPCell totalAmtCell1 = new PdfPCell(new Phrase(SectorLabel.Rows[7].ItemArray[1] + " : " + Data.T_BRANCH.BranchName, tableFont));
            totalAmtCell1.Border = Rectangle.NO_BORDER;
            totalAmtCell1.PaddingBottom = 20f;
            itemTable.AddCell(totalAmtCell1);
            PdfPCell totalAmtCell2 = new PdfPCell(new Phrase(SectorLabel.Rows[8].ItemArray[1] + " : " + string.Format("{0:N2}", Data.DemandAmount) + " ₺", tableFont));
            totalAmtCell2.Border = Rectangle.NO_BORDER; //Rectangle.NO_BORDER; //Rectangle.TOP_BORDER;
            totalAmtCell2.PaddingBottom = 20f;
            itemTable.AddCell(totalAmtCell2);

            PdfPCell cellbas1 = new PdfPCell(new Phrase(SectorLabel.Rows[9].ItemArray[1].ToString(), boldTableFont));
            cellbas1.Colspan = 4;
            cellbas1.HorizontalAlignment = 0;
            cellbas1.PaddingBottom = 5f;
            cellbas1.Border = Rectangle.NO_BORDER;
            itemTable.AddCell(cellbas1);
            
            PdfPCell cell = new PdfPCell(new Phrase(Data.FileDescription, tableFont));
            cell.Colspan = 4;
            cell.HorizontalAlignment = 0;
            cell.PaddingBottom = 20f;
            cell.Border = Rectangle.NO_BORDER;
            itemTable.AddCell(cell);
            
            PdfPCell cellbas2 = new PdfPCell(new Phrase(SectorLabel.Rows[12].ItemArray[1].ToString(), boldTableFont));
            cellbas2.Colspan = 4;
            cellbas2.HorizontalAlignment = 0;
            cellbas2.PaddingBottom = 5f;
            cellbas2.Border = Rectangle.NO_BORDER;
            itemTable.AddCell(cellbas2);

            PdfPCell cell2 = new PdfPCell(new Phrase(Data.DoctorDescription, tableFont));
            cell2.Colspan = 4;
            cell2.HorizontalAlignment = 0;
            cell2.PaddingBottom = 20f;
            cell2.Border = Rectangle.NO_BORDER;
            itemTable.AddCell(cell2);
            
            pdfDoc.Add(itemTable);


            PdfContentByte cb = new PdfContentByte(writer);


            BaseFont bf = BaseFont.CreateFont(BaseFont.HELVETICA, BaseFont.CP1250, true);
            cb = new PdfContentByte(writer);
            cb = writer.DirectContent;
            cb.BeginText();
            cb.SetFontAndSize(bf, 8);
            cb.SetTextMatrix(pageSize.GetLeft(270), 20);
            cb.ShowText("http://www.ikincigoz.net");
            cb.EndText();

            //Move the pointer and draw line to separate footer section from rest of page
            cb.MoveTo(40, pdfDoc.PageSize.GetBottom(50));
            cb.LineTo(pdfDoc.PageSize.Width - 40, pdfDoc.PageSize.GetBottom(50));
            cb.Stroke();

            pdfDoc.Close();
            DownloadPDF(PDFData, Data.Id);


        }

        public static void DownloadPDF(System.IO.MemoryStream PDFData, long Id)
        {
            // Clear response content & headers
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ClearContent();
            HttpContext.Current.Response.ClearHeaders();
            HttpContext.Current.Response.ContentType = "application/pdf";
            HttpContext.Current.Response.Charset = string.Empty;
            HttpContext.Current.Response.Cache.SetCacheability(System.Web.HttpCacheability.Public);
            HttpContext.Current.Response.AddHeader("Content-Disposition", string.Format("attachment;filename=IkinciGoz-DosyaRapor-{0}.pdf", Id));
            HttpContext.Current.Response.OutputStream.Write(PDFData.GetBuffer(), 0, PDFData.GetBuffer().Length);
            HttpContext.Current.Response.OutputStream.Flush();
            HttpContext.Current.Response.OutputStream.Close();
            HttpContext.Current.Response.End();
        }
    }
}