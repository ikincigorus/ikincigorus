﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using Quartz;

namespace IkinciGorus.Models
{
    public class _JobAssignClass : IJob
    {
        private readonly ContextDb db = new ContextDb();
        private static string url = "http://online.ikincigoz.net";

        [ErrorLog]
        public void Execute(IJobExecutionContext context)
        {
            List<T_FILES> Files = db.T_FILES.Where(t => t.RollBackFile == false && DbFunctions.AddHours(t.CreateDate, 1) < DateTime.Now && t.Status == 2).ToList();
            T_USER Users = new T_USER();

            foreach (var item in Files)
            {
                if (item.WorkingUserId == null)
                    Users = db.T_USER.Where(t => t.BranchUserId == item.BranchFileId && t.UserSectorId == item.FilesSectorId && t.Blocked == false && t.Status == 1).OrderBy(t => t.T_FILES.Count).FirstOrDefault();
                else
                    Users = db.T_USER.Where(t => t.BranchUserId == item.BranchFileId && t.UserSectorId == item.FilesSectorId && t.Id != item.WorkingUserId && t.Blocked == false && t.Status == 1).OrderBy(t => t.T_FILES.Count).FirstOrDefault();

                if (Users != null)
                {
                    item.Status = 3;
                    item.WorkingUserId = Users.Id;
                    item.FileRead = true;
                    item.UpdateDate = DateTime.Now;
                    db.SaveChanges();
                    _Common.SendMail(Users.Email, "İkinci Göz : Üzerinize atanan dosya", "Merhaba " + Users.Name + "<br><br>Aşağıda linke tıklayarak üzerinize atanan dosyayı görebilirsiniz.<br><br>Dosyayı görüntülemek için <a href='" + url + "/Expert/FilesDetails/" + item.Id + "'>tıklayın</a>");
                }
                else
                {
                    _Common.SystemSendMail("İkinci Göz : Job Assign", "İkinci göz job assign hiç data bulamadı.");
                }
            }
        }

    }
}