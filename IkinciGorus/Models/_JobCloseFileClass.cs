﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web.Configuration;

namespace IkinciGorus.Models
{
    public class _JobCloseFileClass : IJob
    {
        private readonly ContextDb db = new ContextDb();

        [ErrorLog]
        public void Execute(IJobExecutionContext context)
        {
            List<T_FILES> Files = db.T_FILES.Where(t => t.Status == 6).ToList();
            foreach (var item in Files)
            {
                if (item.UpdateDate.Value.AddDays(item.T_CUSTOMER.ValorDay) < DateTime.Now)
                {
                    T_USER Users = db.T_USER.Where(t => t.Id == item.WorkingUserId).FirstOrDefault();
                    item.Status = 7;
                    //item.WorkingUserId = null;
                    item.FileRead = true;
                    item.EvaluatedAmount = item.DemandAmount - (item.DemandAmount * item.T_CUSTOMER.DiscountRate / 100);
                    item.UpdateDate = DateTime.Now;
                    db.SaveChanges();
                    _Common.SendMail(Users.Email, "İkinci Göz : Dosya Kapanması", "Merhaba " + Users.Name + "<br><br>Değerlendirilen " + item.Id + " numaralı dosyanız " + item.T_CUSTOMER.ValorDay + " gün içinde kapatılmadığı için otomatik olarak kapatılmıştır.<br><br>İyi günler dileriz.");
                }
            }
        }
    }
}