﻿using Quartz;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Web.Configuration;

namespace IkinciGorus.Models
{
    public class _JobRejectClass : IJob
    {
        private readonly ContextDb db = new ContextDb();

        [ErrorLog]
        [Obsolete]
        public void Execute(IJobExecutionContext context)
        {
            int day = Convert.ToInt16(WebConfigurationManager.AppSettings["jobRejectedDay"]);
            List<T_FILES> Files = db.T_FILES.Where(t => t.Status == 3 && EntityFunctions.AddDays(t.UpdateDate, day) < DateTime.Now).ToList();
            
            foreach (var item in Files)
            {
                T_USER Users = db.T_USER.Where(t => t.Id == item.WorkingUserId).FirstOrDefault();
                item.Status = 2;
                //item.WorkingUserId = null;
                item.FileRead = true;
                item.UpdateDate = DateTime.Now;
                db.SaveChanges();
                _Common.SendMail(Users.Email, "İkinci Göz : Üzerinize atanan dosyanın iptali", "Merhaba " + Users.Name + "<br><br>Üzerinize atanan dosyayı " + day + " gün içinde sonuçlandırmadığınız için geri çekilmiştir.<br><br>İyi günler dileriz.");
            }
        }
    }
}