﻿using Quartz;
using Quartz.Impl;
using System;
using System.Web.Configuration;

namespace IkinciGorus.Models
{
    public class _JobScheduler
    {
        public static void Start()
        {
            IScheduler scheduler = StdSchedulerFactory.GetDefaultScheduler();
            scheduler.Start();

            IJobDetail JobAssign = JobBuilder.Create<_JobAssignClass>().Build();
            ITrigger TriggerAssign = TriggerBuilder.Create().WithSimpleSchedule(x => x.WithIntervalInMinutes(Convert.ToInt16(WebConfigurationManager.AppSettings["jobAssignMinutes"])).RepeatForever()).Build();
            scheduler.ScheduleJob(JobAssign, TriggerAssign);

            IJobDetail JobReject = JobBuilder.Create<_JobRejectClass>().Build();
            ITrigger TriggerReject = TriggerBuilder.Create().WithSimpleSchedule(x => x.WithIntervalInHours(Convert.ToInt16(WebConfigurationManager.AppSettings["jobRejectedHours"])).RepeatForever()).Build();
            scheduler.ScheduleJob(JobReject, TriggerReject);

            IJobDetail JobCloseFile = JobBuilder.Create<_JobCloseFileClass>().Build();
            ITrigger TriggerCloseFile = TriggerBuilder.Create().WithSimpleSchedule(x => x.WithIntervalInHours(Convert.ToInt16(WebConfigurationManager.AppSettings["jobRejectedHours"])).RepeatForever()).Build();
            scheduler.ScheduleJob(JobCloseFile, TriggerCloseFile);
        }
    }
}