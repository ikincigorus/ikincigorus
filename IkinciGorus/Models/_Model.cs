﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace IkinciGorus.Models
{
    public class _Model
    {
        public T_CUSTOMER M_CUSTOMER { get; set; }
        public List<T_CUSTOMER> M_CUSTOMERS { get; set; }
        public T_CUSTOMER_STATS M_CUSTOMER_STAT { get; set; }
        public List<T_CUSTOMER_STATS> M_CUSTOMER_STATS { get; set; }
        public T_USER M_USER { get; set; }
        public T_USER M_USER_LOGIN { get; set; }
        public T_USER M_USER_PASSWORD { get; set; }
        public List<T_USER> M_USERS { get; set; }
        public T_USER_STATS M_USER_STAT { get; set; }
        public List<T_USER_STATS> M_USER_STATS { get; set; }
        public T_BRANCH M_BRANCH { get; set; }
        public List<T_BRANCH> M_BRANCHS = new List<T_BRANCH>();
        public T_DOCUMENTS M_DOCUMENT { get; set; }
        public List<T_DOCUMENTS> M_DOCUMENTS { get; set; }
        public T_FILES M_FILE { get; set; }
        public List<T_FILES> M_FILES { get; set; }
        public List<T_FILES> M_BROADCAST_FILES { get; set; }
        public T_HOSPITAL M_HOSPITAL { get; set; }
        public List<T_HOSPITAL> M_HOSPITALS { get; set; }
        public T_MATERIAL M_MATERIAL { get; set; }
        public List<T_MATERIAL> M_MATERIALS { get; set; }
        public T_PRICEPROTOCOL M_PRICEPROTOCOL { get; set; }
        public List<T_PRICEPROTOCOL> M_PRICEPROTOCOLS = new List<T_PRICEPROTOCOL>();
        public T_PRICEPROCCALCTYPE M_PRICEPROCCALCTYPE { get; set; }
        public List<T_PRICEPROCCALCTYPE> M_PRICEPROCCALCTYPES { get; set; }
        public T_CITY M_CITY { get; set; }
        public List<T_CITY> M_CITYS { get; set; }
        public T_TOWN M_TOWN { get; set; }
        public List<T_TOWN> M_TOWNS { get; set; }
        public T_STATUS M_STATU { get; set; }
        public List<T_STATUS> M_STATUS { get; set; }
        public T_NEW_USER_REQUEST M_NEW_USER_REQUEST { get; set; }
        public List<T_NEW_USER_REQUEST> M_NEW_USER_REQUESTS { get; set; }
        public T_SECTOR M_SECTOR { get; set; }
        public List<T_SECTOR> M_SECTORS { get; set; }
        public T_TITLE M_TITLE { get; set; }
        public List<T_TITLE> M_TITLES { get; set; }
        public T_CUSTOMER_OWNER M_CUSTOMER_OWNER { get; set; }
        public List<T_CUSTOMER_OWNER> M_CUSTOMER_OWNERS { get; set; }
        public T_ADVICE M_ADVICE { get; set; }
        public List<T_ADVICE> M_ADVICES { get; set; }

        /*
        public DataTable M_SECTORLISTS = new DataTable();
        public DataTable M_SECTORCHECKS = new DataTable();
        public DataTable M_TITLELISTS = new DataTable();
        */

        public DataTable M_SIGNSTATUS = new DataTable();

        public DataTable M_COMMUNICATIONCHOICE = new DataTable();
        
        public DataTable M_SECTORLABEL = new DataTable();

        public bool? Blocked { get; set; }
        public int? SectorId { get; set; }
        public int Signup { get; set; }


        public string link { get; set; }
        public string page { get; set; }


        public string refUrl { get; set; }
        public string Controller { get; set; }
        public int RejectDay { get; set; }

        public long? StatsRound { get; set; }

        public string CurrUserGuid = _Users.CurrentUserGuid;
        public int CurrTitle = _Users.CurrentTitle;
        public string CurrName = _Users.CurrentName;
        public string CurrEmail = _Users.CurrentEmail;
        public int CurrId = _Users.CurrentId;
        public int CurrGender = _Users.CurrentGender;
        public bool CurrManage = _Users.CurrentManage;
        public int CurrCustId = _Users.CurrentCustomerId;
        public int CurrSectorId = _Users.CurrentSectorId;
        public bool CurrCustomerOwner = _Users.CurrentCustomerOwner;

        public decimal? ProcessPayment { get; set; }
        public int ClosedFiles { get; set; }
        public long? Rating { get; set; }
        public decimal? CostPayment { get; set; }


        public DataTable M_REPORT_PROGRESS = new DataTable();
        public string month1 { get; set; }
        public string currency1 { get; set; }

        public DataTable M_REPORT_CUMULATED = new DataTable();
        public string month2 { get; set; }
        public string currency2 { get; set; }

        public string activeBtn { get; set; }

        public long[] owneruserid { get; set; }
        public int[] ownerrate { get; set; }

        public string term { get; set; }

        public decimal totalprice1 { get; set; }
        public decimal totalprice2 { get; set; }
        public double totalprice { get; set; }
        public double? totalcustown { get; set; }

    }
}