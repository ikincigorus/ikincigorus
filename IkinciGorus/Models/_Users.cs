﻿using System;
using System.Web;

namespace IkinciGorus.Models
{
    public static class _Users
    {
        public static string CurrentUserGuid
        {
            get
            {
                string UserGuid = "";

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    UserGuid = HttpContext.Current.User.Identity.Name.Split('~')[0];
                }

                return UserGuid;
            }
        }

        public static int CurrentTitle
        {
            get
            {
                int Title = 0;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    Title = Convert.ToInt32(HttpContext.Current.User.Identity.Name.Split('~')[1]);
                }

                return Title;
            }
        }

        public static string CurrentName
        {
            get
            {
                string Name = "";

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    Name = HttpContext.Current.User.Identity.Name.Split('~')[2];
                }

                return Name;
            }
        }

        public static string CurrentEmail
        {
            get
            {
                string Email = "";

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    Email = HttpContext.Current.User.Identity.Name.Split('~')[3];
                }

                return Email;
            }
        }

        public static int CurrentId
        {
            get
            {
                int Id = 0;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    Id = Convert.ToInt32(HttpContext.Current.User.Identity.Name.Split('~')[4]);
                }

                return Id;
            }
        }

        public static int CurrentGender
        {
            get
            {
                int Gender = -1;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    Gender = Convert.ToInt32(HttpContext.Current.User.Identity.Name.Split('~')[5]);
                }

                return Gender;
            }
        }

        public static bool CurrentManage
        {
            get
            {
                bool Manage = false;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    Manage = Convert.ToBoolean(HttpContext.Current.User.Identity.Name.Split('~')[6]);
                }

                return Manage;
            }
        }

        public static int CurrentCustomerId
        {
            get
            {
                int CustomerId = 0;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    CustomerId = Convert.ToInt32(HttpContext.Current.User.Identity.Name.Split('~')[7]);
                }

                return CustomerId;
            }
        }

        public static int CurrentSectorId
        {
            get
            {
                int SectorId = 0;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    SectorId = Convert.ToInt32(HttpContext.Current.User.Identity.Name.Split('~')[8]);
                }

                return SectorId;
            }
        }

        public static bool CurrentCustomerOwner
        {
            get
            {
                bool CustomerOwner = false;

                if (HttpContext.Current.Request.IsAuthenticated)
                {
                    CustomerOwner = Convert.ToBoolean(HttpContext.Current.User.Identity.Name.Split('~')[9]);
                }

                return CustomerOwner;
            }
        }
    }
}